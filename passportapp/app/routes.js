var jwt = require('jsonwebtoken');

module.exports = function(app, passport) {

// normal routes ===============================================================

    // show the home page (will also have our login links)
    app.get('/', function(req, res) {
        var authCookie = req.cookies['x-cnnApi-authorization'];
        if(authCookie){
            res.redirect('/cnnApp/')
        }
        res.render('index.ejs');
    });

    // PROFILE SECTION =========================
    //app.get('/profile', isLoggedIn, function(req, res) {
    //    res.render('profile.ejs', {
    //        user : req.user
    //    });
    //});

    // LOGOUT ==============================
    app.get('/logout', function(req, res) {
        res.clearCookie("x-cnnApi-authorization");
        req.logout();
        res.redirect('/');
    });

// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

    // locally --------------------------------
        // LOGIN ===============================
        // show the login form
        app.get('/login', function(req, res) {
            res.render('login.ejs', { message: req.flash('loginMessage') });
        });

        // process the login form
        app.post('/login', passport.authenticate('local-login', {
            //successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }),function(req,res){
             console.log("ddddddd")
             var user = req.user;

            var isAdmin = (user.admin!=undefined && user.admin ==true)?true:false;
            var aud = (isAdmin)?'admin@cnnApi.com':"user@cnnApi.com"

            var mytoken = {
                aud:aud,
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:user.local.email,
                sub:user._id//This is the user id
            }

            var token = jwt.sign(mytoken, "testme");
            res.cookie("x-cnnApi-authorization", "Bearer " + token,{maxAge: 86400000});
            res.redirect('/cnnApp/');
        });

        // SIGNUP =================================
        // show the signup form
        app.get('/signup', function(req, res) {
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        });

        // process the signup form
        app.post('/signup', passport.authenticate('local-signup', {
            //successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/signup', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }),function(req,res){
            console.log("ddddddd")
            var user = req.user;

            var isAdmin = (user.admin!=undefined && user.admin ==true)?true:false;
            var aud = (isAdmin)?'admin@cnnApi.com':"user@cnnApi.com"

            var mytoken = {
                aud:aud,
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:user.local.email,
                sub:user._id//This is the user id
            }

            var token = jwt.sign(mytoken, "testme");
            res.cookie("x-cnnApi-authorization", "Bearer " + token,{maxAge: 86400000});
            res.redirect('/cnnApp/');
        });

    // facebook -------------------------------

        // send to facebook to do the authentication
        app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

        // handle the callback after facebook has authenticated the user
        app.get('/auth/facebook/callback',
            passport.authenticate('facebook', {
                //successRedirect : '/cnnApp/',
                failureRedirect : '/'
            }),function(req,res){
                console.log("ddddddd")
                var user = req.user;
                //Create an authrozation header
                var mytoken = {
                    aud:"user@cnnApi.com",
                    exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                    iat:Math.floor(new Date() / 1000),
                    email:user.facebook.email,
                    sub:user._id//This is the user id
                }

                var token = jwt.sign(mytoken, "testme");
                res.cookie("x-cnnApi-authorization", "Bearer " + token,{maxAge: 86400000});
                res.redirect('/cnnApp/');
            });

    // twitter --------------------------------

        // send to twitter to do the authentication
        app.get('/auth/twitter', passport.authenticate('twitter', { scope : 'email' }));

        // handle the callback after twitter has authenticated the user
        app.get('/auth/twitter/callback',
            passport.authenticate('twitter', {
                //successRedirect : '/profile',
                failureRedirect : '/'
            }),function(req,res){
                console.log("ddddddd")
                var user = req.user;
                //Create an authrozation header
                var mytoken = {
                    aud:"user@cnnApi.com",
                    exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                    iat:Math.floor(new Date() / 1000),
                    email:user.twitter.username,
                    sub:user._id//This is the user id
                }

                var token = jwt.sign(mytoken, "testme");
                res.cookie("x-cnnApi-authorization", "Bearer " + token,{maxAge: 86400000});
                res.redirect('/cnnApp/');
            });


    // google ---------------------------------

        // send to google to do the authentication
        app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

        // the callback after google has authenticated the user
        app.get('/auth/google/callback',
            passport.authenticate('google', {
                //successRedirect : '/profile',
                failureRedirect : '/'
            }),function(req,res){
                console.log("ddddddd")
                var user = req.user;
                //Create an authrozation header
                var mytoken = {
                    aud:"user@cnnApi.com",
                    exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                    iat:Math.floor(new Date() / 1000),
                    email:user.google.email,
                    sub:user._id//This is the user id
                }

                var token = jwt.sign(mytoken, "testme");
                res.cookie("x-cnnApi-authorization", "Bearer " + token,{maxAge: 86400000});
                res.redirect('/cnnApp/');
            });



    // LinkedIn --------------------------------

    // send to twitter to do the authentication
    app.get('/auth/linkedin', passport.authenticate('linkedin'));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/linkedin/callback',
        passport.authenticate('linkedin', {
            //successRedirect : '/c',
            failureRedirect : '/'
        }),function(req,res){
            console.log("ddddddd")
            var user = req.user;
            //Create an authrozation header
            var mytoken = {
                aud:"user@cnnApi.com",
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:user.linkedin.email,
                sub:user._id//This is the user id
            }

            var token = jwt.sign(mytoken, "testme");
            res.cookie("x-cnnApi-authorization", "Bearer " + token,{maxAge: 86400000});
            res.redirect('/cnnApp/');
        });


// =============================================================================
// TOKEN BASED AUTHENTiCATION (FROM MOBILE CLIENTS) =============
// =============================================================================

    //LOCAL
    app.post('/auth/local/login',
        passport.authenticate('local-login'),
        function(req, res) {
            console.log("local-login'")
            var user = req.user;
            //Create an authrozation header
            var mytoken = {
                aud:"user@cnnApi.com",
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:user.local.email,
                sub:user._id//This is the user id
            }

            var token = jwt.sign(mytoken, "testme");
            res.json({Authorization:"Bearer " + token})
        });

    app.post('/auth/local/signup',
        passport.authenticate('local-signup'),
        function(req, res) {
            console.log("local-signup")
            var user = req.user;
            //Create an authrozation header
            var mytoken = {
                aud:"user@cnnApi.com",
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:user.local.email,
                sub:user._id//This is the user id
            }

            var token = jwt.sign(mytoken, "testme");
            res.json({Authorization:"Bearer " + token})
        });



    //GOOGLE
    app.post('/auth/google/token',
        passport.authenticate('google-token'),
        function(req, res) {
            console.log("google-token")
            var user = req.user;
            //Create an authrozation header
            var mytoken = {
                aud:"user@cnnApi.com",
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:user.linkedin.email,
                sub:user._id//This is the user id
            }

            var token = jwt.sign(mytoken, "testme");
            res.json({Authorization:"Bearer " + token})
        });

    //FACEBOOK
    app.post('/auth/facebook/token',
        passport.authenticate('facebook-token'),
        function (req, res) {
            console.log("facebook-token")
            var user = req.user;
            //Create an authrozation header
            var mytoken = {
                aud:"user@cnnApi.com",
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:user.linkedin.email,
                sub:user._id//This is the user id
            }

            var token = jwt.sign(mytoken, "testme");
            res.json({Authorization:"Bearer " + token})
        }
    );

    //LINKEDIN
    app.post('/auth/linkedin/token',
        passport.authenticate('linkedin-token'),
        function (req, res) {
            console.log("linkedin-token")
            var user = req.user;
            //Create an authrozation header
            var mytoken = {
                aud:"user@cnnApi.com",
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:user.linkedin.email,
                sub:user._id//This is the user id
            }

            var token = jwt.sign(mytoken, "testme");
            res.json({Authorization:"Bearer " + token})

        }
    );

    //TWITTER
    app.post('/auth/twitter/token',
        passport.authenticate('twitter-token'),
        function (req, res) {
            console.log("twitter-token")
            var user = req.user;
            //Create an authrozation header
            var mytoken = {
                aud:"user@cnnApi.com",
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:user.linkedin.email,
                sub:user._id//This is the user id
            }

            var token = jwt.sign(mytoken, "testme");
            res.json({Authorization:"Bearer " + token})
        }
    );

// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

    // locally --------------------------------
        app.get('/connect/local', function(req, res) {
            res.render('connect-local.ejs', { message: req.flash('loginMessage') });
        });
        app.post('/connect/local', passport.authenticate('local-signup', {
            successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));

    // facebook -------------------------------

        // send to facebook to do the authentication
        app.get('/connect/facebook', passport.authorize('facebook', { scope : 'email' }));

        // handle the callback after facebook has authorized the user
        app.get('/connect/facebook/callback',
            passport.authorize('facebook', {
                successRedirect : '/profile',
                failureRedirect : '/'
            }));

    // twitter --------------------------------

        // send to twitter to do the authentication
        app.get('/connect/twitter', passport.authorize('twitter', { scope : 'email' }));

        // handle the callback after twitter has authorized the user
        app.get('/connect/twitter/callback',
            passport.authorize('twitter', {
                successRedirect : '/profile',
                failureRedirect : '/'
            }));


    // google ---------------------------------

        // send to google to do the authentication
        app.get('/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));

        // the callback after google has authorized the user
        app.get('/connect/google/callback',
            passport.authorize('google', {
                successRedirect : '/profile',
                failureRedirect : '/'
            }));

// =============================================================================
// UNLINK ACCOUNTS =============================================================
// =============================================================================
// used to unlink accounts. for social accounts, just remove the token
// for local account, remove email and password
// user account will stay active in case they want to reconnect in the future

    // local -----------------------------------
    app.get('/unlink/local', isLoggedIn, function(req, res) {
        var user            = req.user;
        user.local.email    = undefined;
        user.local.password = undefined;
        user.save(function(err) {
            res.redirect('/cnnApp/');
        });
    });

    // facebook -------------------------------
    app.get('/unlink/facebook', isLoggedIn, function(req, res) {
        var user            = req.user;
        user.facebook.token = undefined;
        user.save(function(err) {
            res.redirect('/cnnApp/');
        });
    });

    // twitter --------------------------------
    app.get('/unlink/twitter', isLoggedIn, function(req, res) {
        var user           = req.user;
        user.twitter.token = undefined;
        user.save(function(err) {
            res.redirect('/cnnApp/');
        });
    });

    // google ---------------------------------
    app.get('/unlink/google', isLoggedIn, function(req, res) {
        var user          = req.user;
        user.google.token = undefined;
        user.save(function(err) {
            res.redirect('/cnnApp/');
        });
    });


};

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}
