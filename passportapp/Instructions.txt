CNNAPI
-----------------------------------------------

How To Authorize with the server using Social Login's
------------------------------------------------------------------
In order to authenticate using a mobile client (IOS , Android), it needs to authenticate using the appropriate social network.
After the authentication has been performed the Social network server will send back to the client an access_token and|or refresh_token and the profile of the user
In order to Authenticate with the server the access_token and|or refresh_token must be send to the server using POST request in the below urls with params :
- access_token
- refresh_token (not required )

1) Using Google
http://[domain]/auth/google/token

2) Using Facebook
http://[domain]/auth/facebook/token

3) Using Linkedin
http://[domain]/auth/linkedin/token


The server will respond back with the a JSON object that holds the Authorization token
i.e.
{"Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyQGNubkFwaS5jb20iLCJleHAiOjE0Nj
UzODE3MjcsImlhdCI6MTQ2NTI5NTMyNywic3ViIjoiNTc1NjhkZTNjNWIxMTllODBkYWUxOTVmIn0.GDFK34cavshrz-EEp1FyT0YgaKMdbjw8nloOD0tmCJY"}




Accessing the API
-------------------------------------------------------------------------------------------------------
The client in order to access the Server Api at http://[domain]/cnnApp/ it needs to send in the header of every request the token given by the server
(NOT the token from the social login's)

i.e

headers
Authorization=Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyQGNubkFwaS5jb20iLCJleHAiOjE0Nj
UzODE3MjcsImlhdCI6MTQ2NTI5NTMyNywic3ViIjoiNTc1NjhkZTNjNWIxMTllODBkYWUxOTVmIn0.GDFK34cavshrz-EEp1FyT0YgaKMdbjw8nloOD0tmCJY

The server with every request will respond with the data but will also include a cookie header "x-cnnApi-authorization"
This header includes an updated token that the client will need to save it for the next request.

The token is that is send on every request will expire after 24 hours for security reasons


---------------------
ACCESSING THE DATA
---------------------
==== Articles
GET http://[domain]/cnnApp/api/v1/articles

Headers:
Authorization=Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyQGNubkFwaS5jb20iLCJleHAiOjE0Nj
UzODE3MjcsImlhdCI6MTQ2NTI5NTMyNywic3ViIjoiNTc1NjhkZTNjNWIxMTllODBkYWUxOTVmIn0.GDFK34cavshrz-EEp1FyT0YgaKMdbjw8nloOD0tmCJY


Response:

{
    res: [
    {
    _id: "57533b2280d621c251392a00",
    _date_created: "2016-06-04T20:33:38.713Z",
    _videos: "",
    _category_id: "5753345bf97dfae84fcfcfe7",
    _photos: [
    "http://77.235.55.48/uploads/6b28fc634f4748752a92b75548600795"
    ],
    _description: "test",
    _title: "myarticle",
    _user_id: "57532d671db4cc614f592908"
    },
    {
    _id: "5755407880d621c251392a01",
    _date_created: "2016-06-06T09:20:56.827Z",
    _videos: "",
    _category_id: "5753345bf97dfae84fcfcfe7",
    _photos: [ ],
    _description: "test",
    _title: "asdsada",
    _user_id: "57553fb01db4cc614f592909"
    }
    ],
    msg: "",
    error: false
}

=== Categories
GET http://[domain]/cnnApp/api/v1/categories

Response:

-------------
ADDING DATA
-------------

=== Articles
POST http://[domain]/cnnApp/api/v1/articles

Headers
Content-Type:multipart/form-data
Authorization=Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyQGNubkFwaS5jb20iLCJleHAiOjE0Nj
UzODE3MjcsImlhdCI6MTQ2NTI5NTMyNywic3ViIjoiNTc1NjhkZTNjNWIxMTllODBkYWUxOTVmIn0.GDFK34cavshrz-EEp1FyT0YgaKMdbjw8nloOD0tmCJY

Request Payload
title
category_id
description
uploads

Response:
{"res":
    {"_date_created":"2016-06-07T10:59:44.012Z","_id":"5756a9201f9fcb60172a2584",
    "_videos":"","_category_id":"5753345bf97dfae84fcfcfe7","_photos":[],
    "_description":"ggggg","_title":"ggggggg","_user_id":"57532d671db4cc614f592908"
    },
  "error":null,
  "msg":""
}


=== Categories
POST http://[domain]/cnnApp/api/v1/categories

Headers
Content-Type:application/json
Authorization=Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyQGNubkFwaS5jb20iLCJleHAiOjE0Nj
UzODE3MjcsImlhdCI6MTQ2NTI5NTMyNywic3ViIjoiNTc1NjhkZTNjNWIxMTllODBkYWUxOTVmIn0.GDFK34cavshrz-EEp1FyT0YgaKMdbjw8nloOD0tmCJY

Request Payload
{category: "fsdfsfsdf"}

Response:
{"res":
{"_date_created":"2016-06-07T11:00:54.810Z",
"_id":"5756a9661f9fcb60172a2585",
"_user_id":"57532d671db4cc614f592908",
"_title":"fsdfsfsdf"},
"msg":"","error":false}

--------------
DELETING DATA
--------------

=== Articles
DELETE http://[domain]/cnnApp/api/v1/articles

Headers
Content-Type:application/json
Authorization=Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyQGNubkFwaS5jb20iLCJleHAiOjE0Nj
UzODE3MjcsImlhdCI6MTQ2NTI5NTMyNywic3ViIjoiNTc1NjhkZTNjNWIxMTllODBkYWUxOTVmIn0.GDFK34cavshrz-EEp1FyT0YgaKMdbjw8nloOD0tmCJY

Request Payload
{article_id: "5756a5c11f9fcb60172a2582"}

Response:
{"res":{"ok":1,"n":1},"msg":"","error":false}



=== Categories
DELETE http://[domain]/cnnApp/api/v1/categories

Headers
Content-Type:application/json
Authorization=Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyQGNubkFwaS5jb20iLCJleHAiOjE0Nj
UzODE3MjcsImlhdCI6MTQ2NTI5NTMyNywic3ViIjoiNTc1NjhkZTNjNWIxMTllODBkYWUxOTVmIn0.GDFK34cavshrz-EEp1FyT0YgaKMdbjw8nloOD0tmCJY

Request Payload
{category_id: "5756a5c11f9fcb60172a2582"}


Response:
{"res":{"ok":1,"n":0},"msg":"","error":false}


--------------
UPDATING DATA
--------------


=== Categories
PUT http://[domain]/cnnApp/api/v1/categories

Headers
Content-Type:application/json
Authorization=Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJ1c2VyQGNubkFwaS5jb20iLCJleHAiOjE0Nj
UzODE3MjcsImlhdCI6MTQ2NTI5NTMyNywic3ViIjoiNTc1NjhkZTNjNWIxMTllODBkYWUxOTVmIn0.GDFK34cavshrz-EEp1FyT0YgaKMdbjw8nloOD0tmCJY

Request Payload
{category: "dddddddd", category_id: "5756a5c11f9fcb60172a2582"}

Response:

{"res":{"ok":1,"nModified":0,"n":0},"msg":"","error":false}


