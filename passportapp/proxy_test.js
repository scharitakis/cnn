var http = require('http'),
    httpProxy = require('http-proxy');

//
// Create a proxy server with custom application logic
//
var proxy = httpProxy.createProxyServer({});

//
// Create your custom server and just call `proxy.web()` to proxy
// a web request to the target passed in the options
// also you can use `proxy.ws()` to proxy a websockets request
//
var options = {
    '/app/': 'http://127.0.0.1:3000',
    '/offervault/account': 'http://127.0.0.1:3000',

}


var server = http.createServer(function(req, res) {
    // You can define here your custom logic to handle the request
    // and then proxy the request.
    //console.log(req.url)
    //req.url = req.url.replace("/admin","/mergedAssets");
    console.log('before',req.url)
    //console.log(req);


    if(req.url.indexOf("/cnnApp/")==-1 ){
        proxy.web(req, res, { target:'http://127.0.0.1:3000' });
    }else {
        req.url = req.url.replace("/cnnApp/", '/');
        console.log('after',req.url)
        proxy.web(req, res, {target: 'http://127.0.0.1:3001'});
    }
});

console.log("listening on port localhost")
server.listen(80);