// config/auth.js

var social = (function(){

    var currentEnv = process.env.NODE_ENV || "development";

    var socialLogin = {
        production:{
            'facebookAuth' : {
                'clientID'        : 'your-secret-clientID-here', // your App ID
                'clientSecret'    : 'your-client-secret-here', // your App Secret
                'callbackURL'     : 'http://localhost:8080/auth/facebook/callback'
            },

            'twitterAuth' : {
                'consumerKey'        : 'your-consumer-key-here',
                'consumerSecret'     : 'your-client-secret-here',
                'callbackURL'        : 'http://localhost:8080/auth/twitter/callback'
            },

            'googleAuth' : {
                'clientID'         : '169018901065-08ajomqj5olf3leuj6prveahud5vkevn.apps.googleusercontent.com',
                'clientSecret'     : 'w2WsJZKBbcUZiRNhOG9f9T1H',
                'callbackURL'      : 'http://localhost:3000/auth/google/callback'
            },
            'linkedInAuth' : {
                'clientID'         : '77487y6nfah66u',
                'clientSecret'     : 'e2jTAWi95b6UbwU5',
                'callbackURL'      : 'http://localhost:3000/auth/linkedin/callback'
            }
        },
        development:{
            'facebookAuth' : {
                'clientID'        : '1717847708456059', // your App ID
                'clientSecret'    : '94a0851f3fc1d6abea7d7eb9b920a4d6', // your App Secret
                'callbackURL'     : 'http://api.cnn.gr/auth/facebook/callback'
            },

            'twitterAuth' : {
                'consumerKey'        : 'your-consumer-key-here',
                'consumerSecret'     : 'your-client-secret-here',
                'callbackURL'        : 'http://api.cnn.gr/auth/twitter/callback'
            },

            'googleAuth' : {
                'clientID'         : '40467557747-c28ikbr892504t1ll33d2lg6mu3thdk5.apps.googleusercontent.com',
                'clientSecret'     : 's3rDlvz6i1v5-JblC0r00_5W',
                'callbackURL'      : 'http://api.cnn.gr/auth/google/callback'//'http://77.235.55.48/auth/google/callback'
            },
            'linkedInAuth' : {
                'clientID'         : '77wveh9qjfp7tl',
                'clientSecret'     : 'dV7fY0mEFe6bVEa5',
                'callbackURL'      : 'http://api.cnn.gr/auth/linkedin/callback'
            }
        },
        local:{
            'facebookAuth' : {
                'clientID'        : '908358012572448', // your App ID
                'clientSecret'    : '9283bf6f29195f87dc3ee5419f21b2b2', // your App Secret
                'callbackURL'     : 'http://localhost/auth/facebook/callback'
            },

            'twitterAuth' : {
                'consumerKey'        : 'your-consumer-key-here',
                'consumerSecret'     : 'your-client-secret-here',
                'callbackURL'        : 'http://localhost:3000/auth/twitter/callback'
            },

            /*'googleAuth' : {
                'clientID'         : '169018901065-08ajomqj5olf3leuj6prveahud5vkevn.apps.googleusercontent.com',
                'clientSecret'     : 'w2WsJZKBbcUZiRNhOG9f9T1H',
                'callbackURL'      : 'http://localhost:3000/auth/google/callback'
            },*/
            'googleAuth' : {
                'clientID'         : '15052826144-2s1mmheht41mbeah8kiv0vkb909pcs2v.apps.googleusercontent.com',
                'clientSecret'     : 'NZxs2C2yvdKghxPRPZi3s_Ql',
                'callbackURL'      : 'http://localhost/auth/google/callback'//'http://77.235.55.48/auth/google/callback'
            },
            'linkedInAuth' : {
                'clientID'         : '77487y6nfah66u',
                'clientSecret'     : 'e2jTAWi95b6UbwU5',
                'callbackURL'      : 'http://localhost/auth/linkedin/callback'
            }
        }
    }

    return socialLogin[currentEnv];


})()

module.exports = social;



/*
// expose our config directly to our application using module.exports
module.exports = {

    'facebookAuth' : {
        'clientID'        : 'your-secret-clientID-here', // your App ID
        'clientSecret'    : 'your-client-secret-here', // your App Secret
        'callbackURL'     : 'http://localhost:8080/auth/facebook/callback'
    },

    'twitterAuth' : {
        'consumerKey'        : 'your-consumer-key-here',
        'consumerSecret'     : 'your-client-secret-here',
        'callbackURL'        : 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth' : {
        'clientID'         : '169018901065-08ajomqj5olf3leuj6prveahud5vkevn.apps.googleusercontent.com',
        'clientSecret'     : 'w2WsJZKBbcUZiRNhOG9f9T1H',
        'callbackURL'      : 'http://localhost:3000/auth/google/callback'
    },
    'linkedInAuth' : {
        'clientID'         : '77487y6nfah66u',
        'clientSecret'     : 'e2jTAWi95b6UbwU5',
        'callbackURL'      : 'http://localhost:3000/auth/linkedin/callback'
    }

};
*/
