﻿var jwt = require('jsonwebtoken');
var logger = require("../../utils/logger");
var config = require("../../config/config");
var request = require('request');
var async = require('async');
var moment = require('moment');

function auth(req, res, next) {
    var secret =  req.app.settings.config.secret;  //req.secret; //this is the preview (dev) secret. we must use the production when we deploy to production. see readme.md
    var authCookie = req.cookies['x-cnn-authorization'];
    var jwtToken = unescape(authCookie).split(' ')[1];
    //console.log("auth path = " +req.path+"==="+req.url)
    if(req.path=="/login"|| req.url=="/register" || req.path=="/forgot" || req.path=="/forgot/change" || req.path=="/forgot/change/done" || req.path=="/forgot/sent" ||
        req.path=="/verified" || req.path=="/verification/resend" || req.path=="/verification" ){
        if (!authCookie) {
            next();
            return
        }else{
            jwt.verify(jwtToken, secret, function (err, decoded) {
                //need to find if token has expired
                //console.log(decoded)
                if(err){
                    res.clearCookie("x-cnn-authorization");
                    res.redirect('/login')
                    return
                }
                if(decoded.exp < moment().unix())
                {
                    next();
                    return

                }else{
                    res.redirect('/')
                    return
                }

                //if expired next

                //else redirect to login
            })
        }
    }else if(req.path=="/google" || req.path=="/ldap" || req.path=="/facebook" )
    {
        next();
        return
    }
    else{
        if (!authCookie) {
            res.redirect('/login')
            return
        }else{
            jwt.verify(jwtToken, secret, function (err, decoded) {
                //need to find if token has expired
                //console.log(decoded)
                if(err){
                    res.clearCookie("x-cnn-authorization");
                    res.redirect('/login')
                    return
                }
                var isAdmin = (decoded.aud=="admin@cnn.com")?true:false;
                if(decoded.exp < moment().unix())
                {
                    //if expired redirect to login
                    res.redirect('/login')
                    return;
                }else{
                    req.isAdmin = isAdmin
                    next();
                    return;
                }

            })
        }
    }

    
}

module.exports = auth;