var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var stormpath = require('express-stormpath');
var config = require('./config/config');
var app_config = require('./config/app_config');
var ActiveDirectory = require('activedirectory');
var uuid = require('node-uuid');
var moment = require('moment');
var request = require('request');
var async = require('async');
var jwt = require('jsonwebtoken');
var auth = require('./lib/middleware/auth');

var routes = require('./routes/index');
var ldap = require('./routes/ldap');
var zenauth = require('./routes/zenauth');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


/*var sessionMiddleware = function(req,res,next){
 console.log(req,res,next)
 next();
 }*/
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//var ad = new ActiveDirectory(config.activedirectory);
//app.set('activeDirectory',ad)







var createWebToken =function(user,req, res, next){
  var secret = req.app.settings.config.secret;
  var jwt_exp = req.app.settings.config.jwt.exp;
  var aud = 'user@cnn.com';
  if(user._level==1){
    aud = 'admin@cnn.com'
  }else if(user._level==3) {
    aud = 'salesman@cnn.com';
  }else if(user._level==5){
    aud = 'user@cnn.com'
  }

  var tokenObj  = {
    iss : 'cnn.com',
    aud : aud,
    sub : user._user_id,
    exp : jwt_exp + Math.floor(new Date() / 1000),
    iat : Math.floor(new Date() / 1000),
    email : user._username,
    useas:{},
    apikey : user._apikey
  }
  var token = jwt.sign(tokenObj, secret);
  //res.clearCookie("stormpathSession");
  res.cookie("x-cnn-authorization", "Bearer " + token,{maxAge: 86400000 });
  next();
}


var createNewUser = function(mongo,email,req,res,next){
  var tmpStamp = moment().unix();
  var tmpStampHex = tmpStamp.toString(16);
  var ip = req.remoteIP;

  var getIp2Location = function(clb){
    request('http://ip2loc.minimob.cloud/'+ip, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body) // Show the HTML for the Google homepage.
        return clb(null,JSON.parse(body))
      }
      return clb(error,null);
    })
  }

  var getUserSeq = function(clb){
    mongo.sequences().findOneAndUpdate({"_id":"User"},{ $inc:{"seq":1 }},{new:true},function(err,res1){
      if(!err){
        var user_id = res1.seq
        var _id = user_id.toString(16);
        console.log("increment sequense:"+user_id+' hex '+ _id)
        return clb(null,{user_id:user_id,_id:_id})

      }
      return clb(err,null)
    })
  }



  try {
    async.series({
          user_seq: getUserSeq
        },
        function (err, result) {

          var user_seq = result.user_seq;


          var newUserObj = {
            '_id': user_seq._id,
            '_user_id': user_seq.user_id,
            '_username': email,
            '_modified': tmpStamp,
            '_registration_date': tmpStamp,
            '_level': 5,
            '_registration_addr': req.remoteIP,
            '_apikey': uuid.v4(),
            '_last_login': 0,
            '_permissions': {}
          }


          var newUser = new mongo.user()(newUserObj);
          newUser.save(function(err,userNew){
            if(!err){
              console.log("User created : "+ userNew)
              createWebToken(userNew, req, res, next)
              //next();

            }else{
              console.log("User could not be saved Err "+err)
            }
          })
          console.log(newUser)

        });


  }catch(e){
    console.log(e)
  }

}

var postLogin = function(account, req, res, next,clb){
  console.log("remoteIP :"+ req.remoteIP)
  console.log("postLogin ===== account email"+ account.email)
  var mongo = req.app.get("app_config").mongoCluster.masterRepository;
  var email = account.email.toLowerCase();
  mongo.user().findOne({"_username":email},function(err,user){
    console.log(err, user)
    if(err){
      res.redirect('/login');
      return;
    }
    if(!user){
      //TODO : if user exists in stormpath but not in minimob
      createNewUser(mongo,email,req,res,next)
      //res.redirect('/login');
      //return;
    }else {
      var tmpStamp = moment().unix();

      if (user._apikey != "" && user._apikey != null) {
        mongo.user().update({"_username": email}, {$set: {"_last_login": tmpStamp}}, function (err, raw) {
          createWebToken(user, req, res, next)
        });
      }
      else {
        user._apikey = uuid.v4();
        mongo.user().update({"_username": email}, {
          $set: {
            "_apikey": user._apikey,
            "_last_login": tmpStamp
          }
        }, function (err, raw) {
          if (!err) {
            createWebToken(user, req, res, next) //TODO: check this if working
          }
        });
      }
    }
  })
}

var postRegistration = function(account, req, res, next){
  console.log("postRegistration");
  var tmpStamp = moment().unix();
  var companyName = req.body.company
  req.user.customData.company = companyName;
  req.user.save();
  var mongo = app_config.mongoCluster.randomSlaveRepository();
  console.log('User:', account, 'just registered!');
  if(account.status =="UNVERIFIED"){
    next();
    return;
  }

  var email = account.email.toLowerCase();

  mongo.user().findOne({"_username":email},function(err,user){
    console.log(err, user)
    if(err){
      //If error
      res.redirect('/login');
      return;
    }
    if(user==null) {
      //if user does not exist create One
      createNewUser(mongo,email,req,res,next)
    }
    else
    {
      //If user exists
      if(user._apikey!="" && user._apikey!=null )
      {
        mongo.user().update({ "_username": email }, { $set: { "_last_login": tmpStamp}}, function(err,raw) {
          createWebToken(user, req, res, next)
        });
      }
      else
      {
        user._apikey = uuid.v4();
        mongo.user().update({ _username: account.email }, { $set: { _apikey: user._apikey }}, function(err,raw){
          if(!err){
            createWebToken(user,req, res, next) //TODO: check this if working
          }else{
            res.redirect('/login');
            return;
          }
        });
      }
    }

  })




}

app.set('config',config)
//app.use(config);
app.set("app_config",app_config);



app.use(function (req, res, next) {
  var remoteIps = (req.ips.length) ? req.ips : false || req.connection.remoteAddress;
  if (typeof remoteIps == "string") {
    remoteIps = remoteIps.replace('::ffff:', "");
    if (remoteIps.indexOf("::1") > -1) {
      remoteIps = "127.0.0.1";
    }
  }
  req.remoteIP = remoteIps;
  next();
});

app.use(auth); //Authentication method using jwt

app.use(stormpath.init(app, {
  apiKeyFile:__dirname + '/config/'+ config.stormpath.apiKeyFile,
  application: config.stormpath.application,
  // secretKey: 'some_long_random_string',
  registrationView: __dirname + '/views/register.jade',
  loginView: __dirname + '/views/login.jade',
  forgotPasswordView: __dirname + '/views/forgot.jade',
  forgotPasswordEmailSentView: __dirname + '/views/forgot_email_sent.jade',
  forgotPasswordChangeView: __dirname + '/views/forgot_change.jade',
  forgotPasswordChangeFailedView: __dirname + '/views/forgot_change_failed.jade',
  forgotPasswordCompleteView: __dirname + '/views/forgot_complete.jade',
  accountVerificationEmailSentView: __dirname + '/views/verification_email_sent.jade',
  accountVerificationCompleteView: __dirname + '/views/verification_complete.jade',
  accountVerificationFailedView: __dirname + '/views/verification_failed.jade',
  resendAccountVerificationView: __dirname + '/views/verification_resend.jade',
  //idSiteVerificationFailedView: '',
  googleLoginFailedView: __dirname + '/views/google_login_failed.jade',
  unauthorizedView: __dirname + '/views/unauthorized.jade',
  postLoginHandler: postLogin,
  postRegistrationHandler: postRegistration,
  //redirectUrl: '/test',
  //enableAutoLogin:true,
  expandCustomData: true,
  enableForgotPassword: true,
  enableAccountVerification: true,
  enableGoogle: true,
  enableFacebook:true,
  //googleLoginUrl:'/google',
  social: {
    google: config.stormpath.google,
    facebook:config.stormpath.facebook
  }
  //sessionMiddleware: sessionMiddleware,
}));


//app.use('/ldap', ldap);
app.use('/', routes);
//app.use('/zenauth', zenauth);

//app.use('/unauthorized',stormpath.groupsRequired(['admins']), function(req, res) {
//  res.send('If you can see this page, you must be in the `admins` group!');
//});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

if (process.platform === "win32") {
  var rl = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
  });

  rl.on("SIGINT", function () {
    process.emit("SIGINT");
  });
}

process.on("SIGINT", function () {
  var db = app.get("app_config");
  var connections = Object.keys(db);
  console.log(connections)
  for(var i=0;i<connections.length;i++)
  {
    console.log(connections[i])
    if(connections[i] == "mongoCluster")
    {
      console.log("closing Mongo Connections")
      db[connections[i]].masterRepository.mongoose.connection.close(function () {console.log("closing Mongo Master")});
      var slaveRepositories = db[connections[i]].slaveRepositories
      for(var d=0;d<slaveRepositories.length;d++){
        db[connections[i]].slaveRepositories[d].mongoose.connection.close(function () {console.log("closing Mongo Slave")});
      }
    }
    else if(["redisSiteRepository","redisCacheRepository","redisLocalRepository"].indexOf(connections[i])>-1)
    {
      console.log("closing Redis Connections")
      db[connections[i]].redisClient.end();
    }
    else if(connections[i]=="redisSharding")
    {
      console.log("closing Redis Shard Connections")
      var clients = Object.keys(db[connections[i]].clients_map);
      for(var d=0;d<clients.length;d++){
        connections[i].clients_map[clients[d]].redisClient.end();
      }
    }
  }
  //graceful shutdown
  process.exit();
});

module.exports = app;
