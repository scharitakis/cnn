var routes = function(app){

    var routes_introduction = require('../routes/introduction');
    var routes_invoice = require('../routes/invoice');
    var routes = require('../routes/index');
    var routes_myOffers = require('../routes/myOffers');
    var routes_myReports = require('../routes/myReports');
    var routes_configuration = require('../routes/configuration');
    var routes_api = require('../routes/api');
    var account = require('../routes/account');
    var routes_balance = require('../routes/balance');
    var routes_manageAccounts = require('../routes/manageAccounts');
    var hartbeat = require('../routes/hartbeat');

    var admin_introduction = require('../routes/AdminIntroduction');
    var admin_invoice = require('../routes/AdminInvoice');
    var admin_payment_method = require('../routes/AdminPaymentMethod');
    var admin_approvals = require('../routes/AdminApprovals');
    var admin_moderation = require('../routes/AdminModeration');
    var admin_reports = require('../routes/AdminReports');
    var admin_payments = require('../routes/AdminPayments');
    var admin_routes_availableOffers = require('../routes/AdminAvailableOffers');
    var admin_routes_myOffers = require('../routes/AdminMyOffers');
    var admin_routes_myReports = require('../routes/AdminMyReports');
    var admin_routes_configuration = require('../routes/AdminConfiguration');
    var admin_routes_api = require('../routes/AdminApi');
    var admin_routes_manageAccounts = require('../routes/AdminManageAccounts');
    var admin_routes_store = require('../routes/AdminStore');
    var admin_routes_balance = require('../routes/AdminBalance');
    var admin_routes_management = require('../routes/AdminManagement');
    var advertiserui = require('../routes/advertiserui');

    app.use('/', routes);
    app.use('/Introduction', routes_introduction);
    app.use('/Invoice', routes_invoice);
    app.use('/AvailableOffers', routes);
    app.use('/MyOffers', routes_myOffers);
    app.use('/MyReports', routes_myReports);
    app.use('/Configuration', routes_configuration);
    app.use('/Api', routes_api);
    app.use('/account', account);
    app.use('/Balance', routes_balance);
    app.use('/manageAccounts', routes_manageAccounts);
    app.use('/admin/introduction', admin_introduction);
    app.use('/admin/invoice', admin_invoice);
    app.use('/admin/paymentMethod', admin_payment_method);
    app.use('/admin', admin_approvals);
    app.use('/admin/approvals', admin_approvals);
    app.use('/admin/moderation', admin_moderation);
    app.use('/admin/reports', admin_reports);
    app.use('/admin/payments', admin_payments);
    app.use('/admin/AvailableOffers', admin_routes_availableOffers);
    app.use('/admin/MyOffers', admin_routes_myOffers);
    app.use('/admin/MyReports', admin_routes_myReports);
    app.use('/admin/Configuration', admin_routes_configuration);
    app.use('/admin/Api', admin_routes_api);
    app.use('/admin/manageAccounts', admin_routes_manageAccounts);
    app.use('/admin/store', admin_routes_store);
    app.use('/admin/balance', admin_routes_balance);
    app.use('/admin/management', admin_routes_management);
    app.use('/hartbeat', hartbeat);
    app.use('/advertiserui', advertiserui);

}
module.exports = routes;