

var config = (function(){

    var currentEnv = process.env.NODE_ENV || "production";

    var jwt={
        production:{
            exp:86400
        },
        development:{
            exp:86400
        },
        local:{
            exp:86400
        }
    }

    
    var stormpath = {
        production:{
            application: 'https://api.stormpath.com/v1/applications/7jvUFfRLgVjHLOigvtPJ01', // production
            apiKeyFile:'apiKey-47IUDW6GULCULVWTLWATBRQDX.properties',
            google: {
                clientId: '15052826144-2s1mmheht41mbeah8kiv0vkb909pcs2v.apps.googleusercontent.com',
                clientSecret: 'NZxs2C2yvdKghxPRPZi3s_Ql'
            },
            facebook: {
                appId: '908358012572448',
                appSecret: '9283bf6f29195f87dc3ee5419f21b2b2'
            }
        },
        development: {
            application: 'https://api.stormpath.com/v1/applications/7jvUFfRLgVjHLOigvtPJ01', // dev
            apiKeyFile: 'apiKey-47IUDW6GULCULVWTLWATBRQDX.properties',
            google: {
                clientId: '15052826144-2s1mmheht41mbeah8kiv0vkb909pcs2v.apps.googleusercontent.com',
                clientSecret: 'NZxs2C2yvdKghxPRPZi3s_Ql'
            },
            facebook: {
                appId: '908358012572448',
                appSecret: '9283bf6f29195f87dc3ee5419f21b2b2'
            }
        },
        local:{
            application: 'https://api.stormpath.com/v1/applications/7jvUFfRLgVjHLOigvtPJ01', // local
            apiKeyFile:'apiKey-47IUDW6GULCULVWTLWATBRQDX.properties',
            google: {
                clientId: '15052826144-2s1mmheht41mbeah8kiv0vkb909pcs2v.apps.googleusercontent.com',
                clientSecret: 'NZxs2C2yvdKghxPRPZi3s_Ql'
            },
            facebook: {
                appId: '908358012572448',
                appSecret: '9283bf6f29195f87dc3ee5419f21b2b2'
            }
        }
    }


    var servers = {
        production: "locahost",
        preview: "locahost",
        development: "locahost",
        local: "locahost"
    };


    var secrets ={
        development:"test",
        production:"test",
        preview: "test",
        local: "test"
    }

    var redis = {
        development:{
            site:{host:"localhost",port:6379}

        },
        production:{
            site:{host:"localhost",port:6379}

        },
        local:{
            site:{host:"localhost",port:6379}

        }
    }

    var mongo = {
        development:{
            mongoSlaves : ['mongodb://localhost/cnn'],
            mongoMaster : 'mongodb://localhost/cnn'

        },
        production:{
            mongoSlaves : ['mongodb://localhost/cnn'],
            mongoMaster : 'mongodb://localhost/cnn'
        },
        local:{
            mongoSlaves : ['mongodb://localhost/cnn'],
            mongoMaster : 'mongodb://localhost/cnn'
        }
    }


    return {
        currentEnv:currentEnv,
        secret:secrets[currentEnv],
        server:servers[currentEnv],
        redis:redis[currentEnv],
        mongo:mongo[currentEnv],
        stormpath:stormpath[currentEnv],
        jwt:jwt[currentEnv]
    }

})()



module.exports =config


