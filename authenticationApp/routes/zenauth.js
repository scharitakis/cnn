var express = require('express');
var router = express.Router();
var moment = require('moment');
var uuid = require('node-uuid');
var jwt = require('jsonwebtoken');
var crypto  =require("crypto");

var createWebToken =function(user,req){
  var secret = req.app.settings.config.zenSecret;
  var jwt_exp = req.app.settings.config.jwt.exp;
  var jti = crypto.createHash('md5').update(moment().unix().toString() + Math.random()*Math.pow(2,23)).digest('hex');

  var tokenObj  = {
    typ :'JWT',
    alg:'HS256',
    iat : Math.floor(new Date() / 1000),
    external_id : user.sub,
    exp : jwt_exp + Math.floor(new Date() / 1000),
    email : user.email,
    name: user.email,
    jti : jti
  }
  var token = jwt.sign(tokenObj, secret);
  return token
}

/* GET home page. */
router.get('/', function(req, res, next) {
  var secret =  req.app.settings.config.secret;  //req.secret; //this is the preview (dev) secret. we must use the production when we deploy to production. see readme.md
  var authCookie = req.cookies['x-minimob-authorization'];
  var jwtToken = unescape(authCookie).split(' ')[1];
  var return_url = req.query.return_to;
  jwt.verify(jwtToken, secret, function (err, decoded) {
    //need to find if token has expired
    //var isAdmin = (decoded.aud=="admin@dashboard.minimob.com")?true:false;
    if(decoded.exp < moment().unix())
    {
      //if expired redirect to login
      res.redirect('/login')
      return;
    }else{
      //
      var user = decoded;
      var jwt  = createWebToken(user,req);

      var url = 'http://support.minimob.com/access/jwt?jwt='+jwt+'&return_to='+return_url;
      res.redirect(url)
      return;
    }

  })

});




module.exports = router;
