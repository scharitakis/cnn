var express = require('express');
var router = express.Router();
var moment = require('moment');
var uuid = require('node-uuid');
var jwt = require('jsonwebtoken');

var createWebToken =function(user,req, res, next){
  var secret = req.app.settings.config.secret;
  var jwt_exp = req.app.settings.config.jwt.exp;

  var aud = 'user@dashboard.minimob.com';
  if(user._level==1){
    aud = 'admin@dashboard.minimob.com'
  }else if(user._level==3) {
    aud = 'salesman@dashboard.minimob.com';
  }else if(user._level==5){
    aud = 'user@dashboard.minimob.com'
  }

  var tokenObj  = {
    iss : 'dashboard.minimob.com',
    aud : aud,
    sub : user._user_id,
    exp : jwt_exp + Math.floor(new Date() / 1000),
    iat : Math.floor(new Date() / 1000),
    email : user._username,
    useas:{},
    apikey : user._apikey
  }
  var token = jwt.sign(tokenObj, secret);
  //res.clearCookie("stormpathSession");
  //res.cookie("x-minimob-authorization", "Bearer " + token);
  res.send({err:null,res:'ok',cookie:{name:"x-minimob-authorization",value:"Bearer " + token}});
}

/* GET home page. */
router.post('/', function(req, res, next) {
  var params = req.body
  var ad = req.app.get('activeDirectory');
  var app_config = req.app.get('app_config');
  var mongo = app_config.mongoCluster.randomSlaveRepository();
  var username = params.login;
  var password = params.password;
  var groupName = "MinimobUsers"

  ad.userExists(username, function(err, exists) {
    if (err) {
      console.log('ERROR: ' +JSON.stringify(err));
      return;
    }
    console.log(username + ' exists: ' + exists);

    ad.isUserMemberOf(username, groupName, function(err, isMember) {
      if (err) {
        console.log('ERROR: ' +JSON.stringify(err));
        res.send({err:"Invalid username or password",res:""})
        return;
      }
      console.log(username + ' isMemberOf ' + groupName + ': ' + isMember);
      ad.authenticate(username, password, function(err, auth) {
        if (err) {
          console.log('ERROR: '+JSON.stringify(err));
          //return;
        }

        if (auth) {
          console.log('Authenticated!');
          mongo.user().findOne({"_username":username},function(err,user){
            console.log(err, user)
            if(!user || err){
              //TODO : if user exists in stormpath but not in minimob
              res.send({err:"Invalid username or password",res:""})
              return;
            }
            var tmpStamp = moment().unix();

            if(user._apikey!="" && user._apikey!=null )
            {
              mongo.user().update({ "_username": username }, { $set: { "_last_login": tmpStamp}}, function(err,raw) {
                createWebToken(user, req, res, next)
              });
            }
            else
            {
              user._apikey = uuid.v4();
              mongo.user().update({ "_username": username }, { $set: { "_apikey": user._apikey,"_last_login": tmpStamp}}, function(err,raw){
                if(!err){
                  createWebToken(user,req, res, next) //TODO: check this if working
                }
              });
            }
          })
        }
        else {
          console.log('Authentication failed!');
          res.send({err:"Invalid username or password",res:""})
        }
      });
    });

  });



  /*ad.groupExists(groupName, function(err, exists) {
    if (err) {
      console.log('ERROR: ' +JSON.stringify(err));
      return;
    }

    console.log(groupName + ' exists: ' + exists);
  });



  var groupName = 'MinimobUsers';

  ad.getUsersForGroup(groupName, function(err, users) {
    if (err) {
      console.log('ERROR: ' +JSON.stringify(err));
      return;
    }

    if (! users) console.log('Group: ' + groupName + ' not found.');
    else {
      console.log(JSON.stringify(users));
    }
  });*/




  //res.send('internetq login');
});

module.exports = router;
