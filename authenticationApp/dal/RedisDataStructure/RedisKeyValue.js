/**
 * Created by s.charitakis
 */
var RedisDBEntityKeyValue = function(options){
    this.redisClient = (options && options.client ? options.client : null);

}

RedisDBEntityKeyValue.prototype.get = function(key,callback)
{
    this.redisClient.get(key, function (err, reply) {
        return callback(err, reply);
    });
}

RedisDBEntityKeyValue.prototype.set = function(key,value,callback)
{
    this.redisClient.set(key,value, function (err, reply) {
        return callback(err, reply);
    });
}

RedisDBEntityKeyValue.prototype.setex = function(key,ttl,value,callback)
{
    this.redisClient.setex(key,ttl,value, function (err, reply) {
        return callback(err, reply);
    });
}

RedisDBEntityKeyValue.prototype.expire = function(key,value,callback){

    this.redisClient.expire(key,value, function (err, reply) {
        return callback(err, reply);
    });

}



RedisDBEntityKeyValue.prototype.exists = function(key,callback){

    this.redisClient.exists(key, function (err, reply) {
        return callback(err, reply);
    });

}


RedisDBEntityKeyValue.prototype.incr = function(key,callback){

    this.redisClient.incr(key, function (err, reply) {
        return callback(err, reply);
    });

}

RedisDBEntityKeyValue.prototype.incrby = function(key,value,callback){

    this.redisClient.incrby(key,value, function (err, reply) {
        return callback(err, reply);
    });

}

RedisDBEntityKeyValue.prototype.incrbyMulti = function(keys,callback){

    //keys = {key:"",value:""}
    var keysArray = Object.keys(keys);
    if (!keysArray || !keysArray.length) {
        return callback("keys is empty or null", null);
    } else {
        var results = {};
        var multi = this.redisClient.multi();
        for (var i = 0; i < keysArray.length; i++) {
            var key = keysArray[i];
            var val =  keys[keysArray[i]];
            this.redisClient.incrby(key, val, function (err, reply) {});
        }

        multi.exec(function (err, replies) {
            if (!err) {
                for (var i = 0; i < keys.length; i++) {
                    var key = keysArray[i].key;
                    results[key] = replies[i];
                }
                return callback(null, results);
            }
            else {
                return callback(err, null);
            }
        });

    }

}


RedisDBEntityKeyValue.prototype.getMulti = function (keys, callback) {
    if (!keys || !keys.length) {
        return callback("keys is empty or null", null);
    } else {
        var results = {};
        var multi = this.redisClient.multi();
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            multi.get(key, function (err, reply) {

            });
        }

        multi.exec(function (err, replies) {
            if (!err) {
                for (var i = 0; i < keys.length; i++) {
                    var key = keys[i];
                    results[key] = replies[i];
                }
                return callback(null, results);
            }
            else {
                return callback(err, null);
            }
        });
    }
}

RedisDBEntityKeyValue.prototype.setMulti = function (keyValue, callback) {
    var keys = Object.keys(keyValue);
    if (!keys || !keys.length) {
        return callback("keys is empty or null", null);
    } else {
        var results = {};
        var multi = this.redisClient.multi();
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            multi.set(key,keyValue[key], function (err, reply) {

            });
        }

        multi.exec(function (err, replies) {
            if (!err) {
                for (var i = 0; i < keys.length; i++) {
                    var key = keys[i];
                    results[key] = replies[i];
                }
                return callback(null, results);
            }
            else {
                return callback(err, null);
            }
        });
    }
}

RedisDBEntityKeyValue.prototype.setexMulti = function (keyValue,ttl, callback) {
    var keys = Object.keys(keyValue);
    if (!keys || !keys.length) {
        return callback("keys is empty or null", null);
    } else {
        var results = {};
        var multi = this.redisClient.multi();
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            multi.setex(key,ttl,keyValue[key], function (err, reply) {

            });
        }

        multi.exec(function (err, replies) {
            if (!err) {
                for (var i = 0; i < keys.length; i++) {
                    var key = keys[i];
                    results[key] = replies[i];
                }
                return callback(null, results);
            }
            else {
                return callback(err, null);
            }
        });
    }
}

module.exports = RedisDBEntityKeyValue;