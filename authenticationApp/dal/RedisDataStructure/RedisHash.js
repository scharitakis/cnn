/**
 * Created by s.charitakis
 */

var RedisDBEntityHash = function(options){
    this.redisClient = (options && options.client ? options.client : null);
    this.hash = (options && options.hash ? options.hash : null);

}

RedisDBEntityHash.prototype.GetValueFromHash = function(field,callback)
{
    this.redisClient.hget(this.hash,field, function (err, replies) {
        return callback(err,replies)
    });
}

RedisDBEntityHash.prototype.hdel = function(field,callback)
{
    this.redisClient.hdel(this.hash,field, function (err, replies) {
        return callback(err,replies);
    });
}


RedisDBEntityHash.prototype.hget = function(field,callback)
{
    this.redisClient.hget(this.hash,field, function (err, replies) {
        return callback(err,replies);
    });
}

RedisDBEntityHash.prototype.hset = function(field,value,callback)
{
    this.redisClient.hset(this.hash,field,value, function (err, replies) {
        return callback(err,replies);
    });
}

RedisDBEntityHash.prototype.GetMultiValueFromHash = function (fields,callback) {
    var includeNull = (includeNull!=undefined)? includeNull:true;
    if (!fields || !fields.length) {
        return callback("fields is empty or null", null);
    } else {
        var results = {};
        var multi = this.redisClient.multi();
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            multi.hget(this.hash, field, function (err, reply) {

            });
        }
        
        multi.exec(function (err, replies) {
            if (!err) {
                for (var i = 0; i < fields.length; i++) {
                    var field = fields[i];
                    results[field] = replies[i];
                }
                return callback(null, results);
            }
            else { 
                return callback(err, null);
            }
        });
    }
}


RedisDBEntityHash.prototype.GetHashKeys = function(callback)
{
    this.redisClient.hkeys(this.hash, function (err, replies) {
        return callback(err,replies)
    })
}

RedisDBEntityHash.prototype.GetHashValues = function(callback)
{
    this.redisClient.hvals(this.hash, function (err, reply) {
        return callback(err, reply);
    });
}

RedisDBEntityHash.prototype.GetAll = function(callback)
{
    this.redisClient.hgetall(this.hash, function (err, reply){
        return callback(err,reply);
    });
}


RedisDBEntityHash.prototype.SetEntryInHash = function(field,value,callback)
{
    this.redisClient.hset([this.hash,field,value], function (err, reply) {
        return callback(err, reply);
    });
}


module.exports = RedisDBEntityHash;