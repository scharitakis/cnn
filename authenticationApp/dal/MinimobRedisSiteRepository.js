﻿/**
 * Created by s.charitakis on 1/27/2015.
 */

var logger = require("../utils/logger");
var RedisKeyValue = require('./RedisDataStructure/RedisKeyValue');
var RedisHash = require('./RedisDataStructure/RedisHash');
var RedisSet = require('./RedisDataStructure/RedisSet');
var RedisSortedSets = require('./RedisDataStructure/RedisSortedSets');
var RedisList = require('./RedisDataStructure/RedisList');

var MinimobRedisSiteRepository = function (options) {
    var port = (options && options.port)?options.port:null;
    var host = (options && options.host)?options.host:null;
    
    this.port = port;
    this.host = host;
    this.redis = require('redis');
    this.redisClient = this.redis.createClient(this.port, this.host);

    this.redisClient.on('connect', function () {
        logger.info("Redis Site connection established");
        logger.debug("Redis Site connection established");
        this.connected = true;
    }.bind(this));
    
    this.redisClient.on('error', function (err) {
        logger.error('connection error: ' + err);
        this.connectionError = err;
        this.connected = false;
    }.bind(this));


    var _hash_userid_toparent_userid = null;
    this.hash_userid_toparent_userid = function () {
        if (_hash_userid_toparent_userid)
            return _hash_userid_toparent_userid;
        _hash_userid_toparent_userid =  new RedisHash({
            client: this.redisClient,
            hash: "hash_userid_toparent_userid"});
        return _hash_userid_toparent_userid;
    }.bind(this);




}

module.exports = MinimobRedisSiteRepository;