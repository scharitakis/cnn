/**
 * Created by s.charitakis
 */
var moment = require('moment');
var constants = require('../../config/constants');
var event = {
    imei: null,
    eventtm: null,
    trunc_hour: null,
    trunc_day: null,
    trunc_week: null,
    trunc_month: null,
    trunc_year: null,
    carrier: null,
    tmstamp: null,
    latitude: null,
    packageownerid: null,
    longitude: null,
    packagename: null,
    appkey: null,
    phonemodel: null,
    remote_address: null,
    manufacturer: null,
    networkoperator: null,
    action: null,
    tmzone: null,
    campaigntype: null,
    response_cid: null,
    campaignid: null,
    creativeid: null,
    appid: null,
    event: null,
    isp: null,
    version: null,
    androidsdk: null,
    country: null,
    locale: null,
    connectiontype: null,
    next_poll: null,
    response_crid: null,
    ipcountry: null,
    ishidden: null,
    osversion: null,
    parentid: null,
    networktype: null,
    sharedapp: null,
    installerpackagename: null,
    registrationid: null,
    req_adchannel: null,
    adchannel: null,
    advertiserid: null,
    offerid: null,
    correlatorid: null,
    serverid: null,
    allinstallsources: null,
    playauthorized: null,
    playservicestatus: null,
    sdk2status: null,
    imeisource: null,
    sdkinitialized: null,
    message: null
}

var RedisDBEventSdk = function (options){
    this.redisClient = (options && options.client ? options.client : null);
    this.list = (options && options.list ? options.list : null);
}

RedisDBEventSdk.prototype.store = function (request, imeiData, appData, response, manufacturer, phonemodel, callback) {
    //TODO:finish this;
    
    var now = new moment();
    var newEvent = Object.create(event);
    
    //TODO correlator
    
    newEvent.imei = request.deviceflexid;
    newEvent.eventtm = now.unix();
    newEvent.trunc_hour = now.clone().startOf('hour').unix();
    newEvent.trunc_day = now.clone().startOf('day').unix();
    newEvent.trunc_week = now.clone().startOf('week').unix();
    newEvent.trunc_month = now.clone().startOf('month').unix();
    newEvent.trunc_year = now.clone().startOf('year').unix();
    newEvent.carrier = request.carrier;
    newEvent.tmstamp = now.clone().utc().format() // TODO   $request_obj->get_timestamp_iso,
    newEvent.latitude = request.latitude;
    newEvent.packageownerid = appData? appData._owner_id : 0; // TODO   defined($obj_app) ? $obj_app->get_owner_id : 0,
    newEvent.longitude = request.longitude;
    newEvent.packagename = request.packagename;
    newEvent.appkey = request.appkey;
    newEvent.phonemodel = undefined;
    newEvent.remote_address = request.remote_address;
    newEvent.manufacturer = undefined;
    newEvent.networkoperator = request.networkoperator;
    newEvent.action = request.action_id;
    newEvent.tmzone = now.utcOffset(); // TODO tmzone			=>	$request_obj->get_timestamp()->offset(),
    newEvent.campaigntype = request.objective? request.objective.adtype_id:0;
    newEvent.response_cid = request.correlator ? request.correlator.campaignid : response.reply_code;
    newEvent.campaignid = request.correlator ? request.correlator.campaignid : 0;
    newEvent.creativeid = request.correlator ? request.correlator.creativeid:0;
    newEvent.appid = request.appid;
    newEvent.event = request.event_id;
    newEvent.isp = request.isp;
    newEvent.version = request.version;
    newEvent.androidsdk = request.androidsdk;
    newEvent.country = request.country;
    newEvent.locale = request.locale;
    newEvent.connectiontype = request.connectiontype;
    newEvent.next_poll = moment(response.getMinNextMessageCheck()+moment().unix()).utc().format(); // TODO   DateTime->from_epoch(epoch=>$response_obj->get_min_nextmessagecheck()+time, time_zone=>'UTC')->datetime(),
    newEvent.response_crid = request.correlator ? request.correlator.creativeid:0;
    newEvent.ipcountry = request.ipcountry;
    newEvent.ishidden = imeiData.is_registered && imeiData.is_optimized?1:0;
    newEvent.osversion = request.osversion;
    newEvent.parentid = appData ? appData.parent_id: 0;
    newEvent.networktype = request.getNetworktypeId();
    newEvent.sharedapp = request.advertiser_adchannels && request.advertiser_adchannels.length?1:0;
    newEvent.installerpackagename = request.installerpackagename;
    newEvent.registrationid = imeiData.is_registered ? imeiDatal.registration_id || -1:0;
    newEvent.req_adchannel = request.getReqAdchannelId ? request.getReqAdchannelId() : -1;
    newEvent.adchannel = request.correlator? request.correlator.adchannel : -1;
    newEvent.advertiserid = request.correlator? request.correlator.advertiserid : 0;
    newEvent.offerid = request.correlator? request.correlator.offerid : 0;
    newEvent.correlatorid = request.clickid || 0;
    newEvent.serverid = request.req.hostname;
    newEvent.allinstallsources = request.hasAllInstallSources()?1:0;
    newEvent.playauthorized = request.hasPlayAuthorized() ?1:0;
    newEvent.playservicestatus = request.playServicesStatusId();
    newEvent.sdk2status = request.sdk2StatusId();
    newEvent.imeisource = request.getIdSourceId();
    newEvent.sdkinitialized = imeiData.is_registered && imeiData.is_active?1:0;
    newEvent.message = request.message || '';
    newEvent.manufacturer = manufacturer;
    newEvent.phonemodel = phonemodel;
    

    var value = JSON.stringify(newEvent);
    this.redisClient.rpush(this.list, value,  function (err, reply) {
        return callback(err, reply);
    });

}

RedisDBEventSdk.prototype.llen = function(callback)
{
    this.redisClient.llen(this.list, function (err, reply) {
        return callback(err, reply);
    });
}


module.exports = RedisDBEventSdk;