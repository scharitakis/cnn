﻿var logger = require("../utils/logger");
var util = require('util');
var EventEmitter = require("events").EventEmitter;

var MinimobMongoRepository = function (options) {
    this.connectionString = options.connectionString;
    this.mongoose = require('mongoose');
    this.master =  options.master||false;
    this.connected = false;
    
    this.db = this.mongoose.createConnection(this.connectionString, { keepAlive: 1 });//this.mongoose.connection;

    this.db.on('error', function (err) {
        logger.error('MongoRepository connection error: ' + err);
        logger.debug('MongoRepository connection error: ' + err);
        this.connectionError = err;
        this.connected = false;
    }.bind(this));
    
    this.db.once('open', function () {
        logger.info("MongoRepository connection established : "+ this.connectionString);
        logger.debug("MongoRepository connection established : "+this.connectionString);
        this.connected = true;
    }.bind(this))

    this.db.on('reconnected', function () {
        logger.info("MongoRepository connection established:reconnected : " + this.connectionString);
        logger.debug("MongoRepository connection established:reconnected : " + this.connectionString);
        if(this.master) {
            //this.mongoCluster.reconnectSlaves();
            this.emit('reconnected');
        }
        this.connected = true;
    }.bind(this));

    var userSchema = require('./MongoEntities/user.js');
    var sequencesSchema = require('./MongoEntities/sequences.js');


    var _sequences = null;
    this.sequences = function () {
        if (_sequences)
            return _sequences;
        _sequences = this.db.model('sequences', sequencesSchema);
        return _sequences;
    }.bind(this);

    var _user = null;
    this.user = function () {
        if (_user)
            return _user;
        _user = this.db.model('User', userSchema);
        return _user;
    }.bind(this);


    EventEmitter.call(this);

}

util.inherits(MinimobMongoRepository,EventEmitter);




// Public
module.exports = MinimobMongoRepository;