/**
 * Created by s.charitakis
 */

var mongoose = require('mongoose');
require('mongoose-long')(mongoose);
var  Schema = mongoose.Schema;

var SequencesSchema = new Schema({
    '_id': { type: String, default: "" },
    'seq': { type: Schema.Types.Long}

}, { collection: 'sequences',versionKey:false });

module.exports = SequencesSchema;