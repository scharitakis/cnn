/**
 * Created by s.charitakis
 */

var mongoose = require('mongoose');
require('mongoose-long')(mongoose);
var  Schema = mongoose.Schema;

var UserSchema = new Schema({
    '_id': { type: String, default: "" },
    '_user_id': { type: String, default: "" },
    '_username': { type: String, default: "" },
    '_modified': { type: Schema.Types.Long, default: null },
    '_last_login': { type: Number, default: null },
    '_registration_date': { type: Schema.Types.Long, default: null },
    '_registration_addr': { type: String, default: "" },
    '_apikey': { type: String, default: null},
    '_level': { type: Number, default: null},
    '_permissions': {},
}, { collection: 'User',versionKey:false });




module.exports = UserSchema;