/**
 * Created by s.charitakis on 1/27/2015.
 */


var logger = require("../utils/logger");
var RedisKeyValue = require('./RedisDataStructure/RedisKeyValue');
var RedisHash = require('./RedisDataStructure/RedisHash');
var RedisDBEntityOffers = require('./RedisEntities/RedisDBEntityOffers');

var MinimobRedisCacheRepository = function (options) {
    var port = (options && options.port)?options.port:null;
    var host = (options && options.host)?options.host:null;

    this.port = port;
    this.host = host;
    this.redis = require('redis');
    this.redisClient = this.redis.createClient(this.port, this.host);

    this.redisClient.on('connect', function () {
        logger.info("Redis Cache connection established");
        logger.debug("Redis Cache connection established");
        this.connected = true;
    }.bind(this));

    this.redisClient.on('error', function (err) {
        logger.error('Redis Cache connection error: ' + err);
        this.connectionError = err;
        this.connected = false;
    }.bind(this));

}

module.exports = MinimobRedisCacheRepository;