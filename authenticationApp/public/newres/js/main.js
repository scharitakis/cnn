$(function() {

  $('.goto_top').click(function() {
     $('body').scrollTo($('body') , 2000, {margin:true} );
  });

  $('.goto_howitworks').click(function() {
     $('body').scrollTo($('.howitworks') , 2000, {margin:true, offset: -160} );
  });

  $('.goto_adtypes').click(function() {
     $('body').scrollTo($('.adtypes') , 2000, {margin:true, offset: -80} );
  });

});
