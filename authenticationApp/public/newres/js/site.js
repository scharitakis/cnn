    $(document).ready(function() {

            //$('#nav-main').scrollspy()

            // Localscrolling
            $('#nav-main, .brand').localScroll();
            $('#news, .container').localScroll();

    });

    $(window).load(function() {
        $('.flexslider').flexslider({
            slideshow: false,
            controlNav: false,
            directionNav: true,
            slideshow: true,
            slideshowSpeed: 9000,
            animation: 'fade',
            animationSpeed: 1000,
            start: function(slider) {
                $('section#mainbann .spinner').hide();
                $('section#mainbann .slides').show();
                $('.slides .blue img').addClass('animated rotateInUpRight');
                $('.slides .blue .hellohead').addClass('animated fadeInWave');
                $('.slides .blue .bluetext').addClass('animated fadeIn');
            },
            before: function(slider) {
                $('.slides li').css('background-image', 'none');
                switch(slider.currentSlide) {
                    case 0:
                        $('section#mainbann').css('background-color', '#3996C7');
                        $('.slides .blue img').removeClass('rotateInUpRight').removeClass('animated');

                        $('.slides .blue .bluetext').removeClass('fadeIn').removeClass('animated');
                        break;
                    case 1:
                        $('section#mainbann').css('background-color', '#dd5151');
                        $('.slides .red img').removeClass('bounceInDown').removeClass('animated');

                        $('.slides .red .redtext').removeClass('fadeIn').removeClass('animated');
                        $('.slides .red .redtextback').removeClass('fadeIn').removeClass('animated');
                        break;
                    case 2:
                        $('section#mainbann').css('background-color', '#59636e');
                        $('.slides .grey img').removeClass('bounceInLeft').removeClass('animated');

                        $('.slides .grey .greytext').removeClass('fadeIn').removeClass('animated');
                        $('.slides .grey .greytextback').removeClass('fadeIn').removeClass('animated');
                        break;
                    case 3:
                        $('section#mainbann').css('background-color', '#74be60');
                        $('.slides .green img').removeClass('bounceInRight').removeClass('animated');
                        $('.slides .green .earth').removeClass('fadeIn').removeClass('animated');
                        $('.slides .green .hallo').removeClass('scaleExpand').removeClass('animated');
                        $('.slides .green .aloha').removeClass('scaleExpand').removeClass('animated');
                        $('.slides .green .hola').removeClass('scaleExpand').removeClass('animated');
                        $('.slides .green .russian').removeClass('scaleExpand').removeClass('animated');
                        $('.slides .green .greentext').removeClass('fadeIn').removeClass('animated');
                        $('.slides .green .greentextback').removeClass('fadeIn').removeClass('animated');
                        break;
                    default:
                        break;
                }
            },
            after: function(slider) {
                switch(slider.currentSlide)
                {
                    case 0:
                      $('.slides .red .rockHand').removeClass('rotateInUpLeftNoFade').removeClass('swing').removeClass('animated');
                      $('.slides .red .lighterHand').removeClass('slideInDiagonal').removeClass('swingAlt').removeClass('animated');
                      $('.slides .blue img').addClass('animated rotateInUpRight');
                      $('.slides .blue .hellohead').addClass('animated fadeInWave');
                      $('.slides .blue .bluetext').addClass('animated fadeIn');
                      $('section#mainbann').css('background-color', '#3996C7');
                      window.setTimeout(function() {
                        $('.slides li.blue').css('background-image', 'url(images/slides/Blue-whiteGlow.png)');
                      }, 500);
                      //$('.slides li.blue').css('background-image', 'url(../images/slides/Blue-whiteGlow.png)');
                      $('.flex-direction-nav li a.flex-prev').hide();
                      break;
                    case 1:
                      $('.slides .blue .hellohead').removeClass('wave').removeClass('animated');
                      $('.slides .grey .greyDollar').removeClass('fadeInDownBigWave').removeClass('animated');
                      $('.slides .red img').addClass('animated bounceInDown');
                      $('.slides .red .rockHand').addClass('animated rotateInUpLeftNoFade');
                      $('.slides .red .rockHand').on('webkitAnimationEnd oanimationend msAnimationEnd animationend',
                        function() {
                            $('.slides .red .rockHand').removeClass('rotateInUpLeftNoFade').addClass('swing');
                        });
                      $('.slides .red .lighterHand').addClass('animated slideInDiagonal');
                      $('.slides .red .lighterHand').on('webkitAnimationEnd oanimationend msAnimationEnd animationend',
                        function() {
                            $('.slides .red .lighterHand').removeClass('slideInDiagonal').addClass('swingAlt');
                        });
                      $('.slides .red .redtext').addClass('animated fadeIn');
                      $('.slides .red .redtextback').addClass('animated fadeIn');
                      $('section#mainbann').css('background-color', '#dd5151');
                      window.setTimeout(function() {
                        $('.slides li.red').css('background-image', 'url(images/slides/Red-whiteGlow.png)');
                      }, 500);
                      // $('.slides li.red').css('background-image', 'url(../images/slides/Red-whiteGlow.png)');
                      $('.flex-direction-nav li a.flex-prev').show();
                      break;
                    case 2:
                      $('.slides .red .rockHand').removeClass('rotateInUpLeftNoFade').removeClass('swing').removeClass('animated');
                      $('.slides .red .lighterHand').removeClass('slideInDiagonal').removeClass('swingAlt').removeClass('animated');
                      $('.slides .grey img').addClass('animated bounceInLeft');
                      $('.slides .grey .greyDollar').addClass('animated fadeInDownBigWave');
                      $('.slides .grey .greytext').addClass('animated fadeIn');
                      $('.slides .grey .greytextback').addClass('animated fadeIn');
                      $('section#mainbann').css('background-color', '#59636e');
                      window.setTimeout(function() {
                        $('.slides li.grey').css('background-image', 'url(images/slides/Grey-whiteGlow.png)');
                      }, 500);
                      // $('.slides li.grey').css('background-image', 'url(../images/slides/Grey-whiteGlow.png)');
                      break;
                    case 3:
                      $('.slides .grey .greyDollar').removeClass('fadeInDownBigWave').removeClass('animated');
                      $('.slides .green img').addClass('animated bounceInRight');
                      $('.slides .green .earth').addClass('animated fadeIn');
                      $('.slides .green .hallo').addClass('animated scaleExpand');
                      $('.slides .green .aloha').addClass('animated scaleExpand');
                      $('.slides .green .hola').addClass('animated scaleExpand');
                      $('.slides .green .russian').addClass('animated scaleExpand');
                      $('.slides .green .greentext').addClass('animated fadeIn');
                      $('.slides .green .greentextback').addClass('animated fadeIn');
                      $('section#mainbann').css('background-color', '#74be60');
                      window.setTimeout(function() {
                        $('.slides li.green').css('background-image', 'url(images/slides/Green-whiteGlow.png)');
                      }, 500);
                      // $('.slides li.green').css('background-image', 'url(../images/slides/Green-whiteGlow.png)');
                      break;
                    default:
                      break;
                }
            }
        });
    });

