var tpj=jQuery;

tpj.noConflict();

function fnRedirect(data){
	var userPreferences;
	var hasPref = false;
	var view = '';
	if(data && data.hasOwnProperty('userPreferences')) {
		userPreferences = data.userPreferences;
		hasPref = (userPreferences.hasOwnProperty('introductoryView') && userPreferences.introductoryView!==null) ? true : false;
		if(hasPref){
			view = parseInt(userPreferences.introductoryView);
		}
	}
	
	if(!data.first) {
		//window.location = '/campaign.asp';
		if(hasPref){
			if(view === 0){
				window.location = '/campaign.asp';
			}
			if(view === 1) {
				window.location = '/offervault/';
			}
		}else{
			window.location = '/offervault/introduction';
		}
		
	} else {
		//window.location = '/application.asp';
		window.location = '/offervault/introduction?f=true'; // Display introduction page 
	}
}

function fnCheckAuth(token) {
    tpj.ajax({
        url: "/checkauth.asp",
        type: "post",
        data: {
            access_token: token
        },
        success: function (data, textStatus, jqXHR) {
            if (data.success) {
				window.location = '/offervault/';
				//fnRedirect(data);
            }

            if(data.msgid == 1) {
                window.location = '/deleted.asp';
            }

            if(data.msgid == 2) {
                window.location = '/help.asp';
            }
        },
        error: function (data) {
            alert(data);
        }
    })
}


