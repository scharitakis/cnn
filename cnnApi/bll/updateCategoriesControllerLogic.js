/**
 * Created by s.charitakis
 */

var wait = require('wait.for');
var moment = require('moment');
var logger = require("../utils/logger");
var conf = require('../config/config');

var UpdateCategoriesControllerLogic = function (options) {
    var logMeta = options.logMeta;
    logMeta.facility = "AddCategoriesController";
    this._logMeta = logMeta;
    this.db = options.db;
};


UpdateCategoriesControllerLogic.prototype.startUpdateCategoriesController = function (request, response) {
    var now = moment();
    var user = request.user;
    var user_id = user.sub;
    var isAdmin = (user.aud=="admin@cnnApi.com")?true:false;
    logger.info("startUpdateCategoriesController: "+moment().diff(now,'millisecond'),this._logMeta);
    var dbMongo = this.db.dbMongo;


    var category_id =  request.body.category_id
    if(isAdmin) {
        var Category = {
            _title: request.body.category
        }

        var updateCategory = function (clb) {
            var category = dbMongo.category().update({_id: category_id}, Category, clb);
        }

        var resUpdateCategory = wait.for(updateCategory);

        console.log(resUpdateCategory);

        var resJSON = {res: resUpdateCategory, msg: "", error: false}
        return resJSON;
    }else{
        var resJSON = {res:"", msg: "You are not admin", error: true}
        return resJSON;
    }
}


module.exports = UpdateCategoriesControllerLogic;