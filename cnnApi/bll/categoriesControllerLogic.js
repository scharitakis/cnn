/**
 * Created by s.charitakis
 */

var wait = require('wait.for');
var moment = require('moment');
var logger = require("../utils/logger");
var conf = require('../config/config');

var CategoriesControllerLogic = function (options) {
    var logMeta = options.logMeta;
    logMeta.facility = "CategoriesController";
    this._logMeta = logMeta;
    this.db = options.db;
};


CategoriesControllerLogic.prototype.startCategoriesController = function (request, response) {
    var now = moment();
    this.now = now;
    logger.info("startCategoriesController: "+moment().diff(now,'millisecond'),this._logMeta);

    var dbMongo = this.db.dbMongo;
    var resGetCategories =[];
    
    if(request.query.id){
        var getCategory = function (clb) {
            var categories = dbMongo.category().find({_id:request.query.id},'_title _date_created _user_id', clb);
        }
        resGetCategories = wait.for(getCategory);
    }else {
       var getCategories = function (clb) {
            var categories = dbMongo.category().find({$or:[{_deleted:false},{_deleted:{$exists: false}}]},'_title _date_created _user_id', clb);
        }

        resGetCategories = wait.for(getCategories);
    }

    console.log(resGetCategories);

    var resJSON ={res:resGetCategories,msg:"",error:false}
    return resJSON;
}


module.exports = CategoriesControllerLogic;