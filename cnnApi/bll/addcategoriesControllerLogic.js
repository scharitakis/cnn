/**
 * Created by s.charitakis
 */

var wait = require('wait.for');
var moment = require('moment');
var logger = require("../utils/logger");
var conf = require('../config/config');

var AddCategoriesControllerLogic = function (options) {
    var logMeta = options.logMeta;
    logMeta.facility = "AddCategoriesController";
    this._logMeta = logMeta;
    this.db = options.db;
};


AddCategoriesControllerLogic.prototype.startAddCategoriesController = function (request, response) {
    var now = moment();
    var user = request.user;
    var user_id = user.sub;
    var isAdmin = (user.aud=="admin@cnnApi.com")?true:false;

    logger.info("startAddCategoriesController: "+moment().diff(now,'millisecond'),this._logMeta);

    if(isAdmin) {
        var dbMongo = this.db.dbMongo;

        var newCategory = {
            '_title': request.body.category,
            '_date_created': now.utc().toISOString(),
            '_user_id': user_id
        }

        var createCategory = function (clb) {
            var category = dbMongo.category().create(newCategory, clb);
        }

        var resCreateCategory = wait.for(createCategory);

        console.log(resCreateCategory);

        var resJSON = {res: resCreateCategory, msg: "", error: false}
        return resJSON;
    }else{
        var resJSON = {res:"", msg: "You are not admin", error: true}
        return resJSON;
    }
}


module.exports = AddCategoriesControllerLogic;