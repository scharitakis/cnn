/**
 * Created by s.charitakis
 */

var wait = require('wait.for');
var moment = require('moment');
var logger = require("../utils/logger");
var conf = require('../config/config');
var fs = require('fs-extra');

var DeleteArticlesControllerLogic = function (options) {
    var logMeta = options.logMeta;
    logMeta.facility = "DeleteArticlesController";
    this._logMeta = logMeta;
    this.db = options.db;
};


DeleteArticlesControllerLogic.prototype.startDeleteArticlesController = function (request, response) {
    var now = moment();
    this.now = now;
    var user = request.user;
    var user_id = user.sub;
    var isAdmin = (user.aud=="admin@cnnApi.com")?true:false;
    var dbMongo = this.db.dbMongo;
    logger.info("startDeleteArticlesController: "+moment().diff(now,'millisecond'),this._logMeta);


    var article_id = request.body.article_id;

    if(isAdmin) {
        var qparams = {_id:article_id}
    }else{
        var qparams = {_user_id:user_id,_id:article_id}
    }

    var getArticle = function(clb){
        var article =  dbMongo.article().findOne(qparams,clb);
    }

    var resGetArticle =  wait.for(getArticle);

    if(resGetArticle){
        var files = resGetArticle._photos
        if(files.length){
            for(var i=0;i<files.length;i++)
            {
                var filename =files[i].replace(conf.servers+'/uploads/',"")
                fs.removeSync('public/uploads/'+filename)
            }
        }

        var removeArticle= function(clb)
        {
            var article =  dbMongo.article().remove({_id:article_id},clb);
        }

        var resRemoveArticle =  wait.for(removeArticle);


        var resJSON ={res:resRemoveArticle,msg:"",error:false}
        return resJSON;
    }else{
        var resJSON ={res:"",msg:"no article found",error:true}
        return resJSON;
    }
}


module.exports = DeleteArticlesControllerLogic;