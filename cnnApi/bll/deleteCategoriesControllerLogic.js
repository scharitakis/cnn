/**
 * Created by s.charitakis
 */

var wait = require('wait.for');
var moment = require('moment');
var logger = require("../utils/logger");
var conf = require('../config/config');

var DeleteCategoriesControllerLogic = function (options) {
    var logMeta = options.logMeta;
    logMeta.facility = "DeleteCategoriesController";
    this._logMeta = logMeta;
    this.db = options.db;
};


DeleteCategoriesControllerLogic.prototype.startDeleteCategoriesController = function (request, response) {
    var now = moment();
    this.now = now;
    var user = request.user;
    var user_id = user.sub;
    var isAdmin = (user.aud=="admin@cnnApi.com")?true:false;
    logger.info("startDeleteCategoriesController: "+moment().diff(now,'millisecond'),this._logMeta);

    if(isAdmin) {
        var dbMongo = this.db.dbMongo;

        var removeCategory = function (clb) {
            var category = dbMongo.category().update({_id: request.body.category_id},{$set:{_deleted:true}}, clb);
        }

        var resRemoveCategory = wait.for(removeCategory);

        console.log(resRemoveCategory);

        var resJSON = {res: resRemoveCategory, msg: "", error: false}
        return resJSON;
    }
    else{
        var resJSON = {res:"", msg: "You are not admin", error: true}
        return resJSON;
    }

}


module.exports = DeleteCategoriesControllerLogic;