/**
 * Created by s.charitakis
 */

var wait = require('wait.for');
var moment = require('moment');
var logger = require("../utils/logger");
var conf = require('../config/config');

var ArticlesControllerLogic = function (options) {
    var logMeta = options.logMeta;
    logMeta.facility = "ArticlesController";
    this._logMeta = logMeta;
    this.db = options.db;
};


ArticlesControllerLogic.prototype.startArticlesController = function (request, response) {
    var user = request.user;
    var user_id = user.sub;
    var isAdmin = (user.aud=="admin@cnnApi.com")?true:false;
    var now = moment();
    this.now = now;
    logger.info("startArticlesController: "+moment().diff(now,'millisecond'),this._logMeta);


    var dbMongo = this.db.dbMongo;
    var resGetArticles=[];
    if(request.query.id){
        if(isAdmin) {
            var qparams = {_id:request.query.id}
        }else{
            var qparams = {_user_id:user_id,_id:request.query.id}
        }
        if(request.query.filter){
            qparams['_category_id']=request.query.filter
        }

        var getArticle = function (clb) {
            var article = dbMongo.article().find(qparams, clb);
        }

        resGetArticles = wait.for(getArticle);
    }
    else if(request.query.page && request.query.pagination){
        if(isAdmin) {
            var qparams = {}
        }else{
            var qparams = {_user_id:user_id}
        }

        if(request.query.filter){
            qparams['_category_id']=request.query.filter
        }

        var getArticleCount = function (clb) {

            var articleCount = dbMongo.article().count(qparams).exec(clb);
        }

        var totalArticles = wait.for(getArticleCount);

        var limit = parseInt(request.query.pagination)
        var skip = parseInt(request.query.pagination) * parseInt(request.query.page);



        var getPaginatedArticles = function (clb) {
            var articleCount = dbMongo.article().find(qparams, clb).limit(limit).skip(skip)
        }

        var getPaginatedArticlesSort = function (clb) {
            var articleCount = dbMongo.article().find(qparams, clb).limit(limit).skip(skip).sort({sortBy:sort})
        }

        if(request.query.sortBy && request.query.sort){
            var sortBy = request.query.sortBy;
            var sort = (request.query.sort=="asc")?1:-1
            var paginatedData = wait.for(getPaginatedArticlesSort);
        }else{
            var paginatedData = wait.for(getPaginatedArticles);
        }


        resGetArticles ={
                            total:totalArticles,
                            page:request.query.page,
                            pagination:request.query.pagination,
                            data:paginatedData
                        }

    }
    else {

        if(isAdmin) {
            var qparams = {}
        }else{
            var qparams = {_user_id:user_id}
        }

        if(request.query.filter){
            qparams['_category_id']=request.query.filter
        }

        var getArticles = function (clb) {
            var articles = dbMongo.article().find(qparams, clb);
        }

        resGetArticles = wait.for(getArticles);
    }

    console.log(resGetArticles);

    var resJSON ={res:resGetArticles,msg:"",error:false}
    return resJSON;
}


module.exports = ArticlesControllerLogic;