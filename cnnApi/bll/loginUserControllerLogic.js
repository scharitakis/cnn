/**
 * Created by s.charitakis
 */

var wait = require('wait.for');
var moment = require('moment');
var logger = require("../utils/logger");
var conf = require('../config/config');
var jwt = require('jsonwebtoken');

var loginUserControllerLogic = function (options) {
    var logMeta = options.logMeta;
    logMeta.facility = "login User Controller";
    this._logMeta = logMeta;
    this.db = options.db;
};



loginUserControllerLogic.prototype.startLoginUserController = function (request, response) {
    var now = moment();
    logger.info("startLoginUserController: "+moment().diff(now,'millisecond'),this._logMeta);
    //var user = request.user;
    //var user_id = user.sub;
    var dbMongo = this.db.dbMongo;
    var params = request.body;
    var type = Object.keys(params)[0];
    var secret =  config.secret;

    var user = params;
    var findUserParams = {}

    if(type == 'local'){
        findUserParams ={'local.email':params.local.email}
    }
    else if(type == 'facebook'){
        findUserParams ={'facebook.id':params.facebook.id}
    }
    else if(type == 'twitter'){
        findUserParams ={'twitter.id':params.twitter.id}
    }
    else if(type == 'google'){
        findUserParams ={'google.id':params.google.id}
    }
    else if(type == 'linkedin'){
        findUserParams ={'linkedin.id':params.linkedin.id}
    }
    else{
        return {error:true,msg:"No Appropriate credentials"}
    }

    var findUser = function(clb){
        dbMongo.user().findOne(findUserParams,clb);
    }

    var resfindUser =  wait.for(findUser);

    if(resfindUser){
        //return authorization header
        var email = ""
        if(type =='twitter'){
            email = resfindUser[type].username;
        }else{
            email = resfindUser[type].email;
        }

        var mytoken = {
            aud:aud,
            exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
            iat:Math.floor(new Date() / 1000),
            email:email,
            sub:resfindUser._id//This is the user id
        }

        var updated_token = jwt.sign(mytoken, secret);
        response.append("Authorization",updated_token);
        return {res:"Authorized",msg:"",error:false};


    }else{
        var createUser = function(clb){
            dbMongo.user().create(user,clb);
        }

        var rescreateUser =  wait.for(createUser);
        if(rescreateUser){
            //return authorization header

            if(type =='twitter'){
                email = user[type].username;
            }else{
                email = user[type].email;
            }

            var mytoken = {
                aud:aud,
                exp: 86400 + Math.floor(new Date() / 1000),// one day 24 hours
                iat:Math.floor(new Date() / 1000),
                email:email,
                sub:user._id//This is the user id
            }

            var updated_token = jwt.sign(mytoken, secret);
            response.append("Authorization",updated_token);
            return {res:"Authorized",msg:"",error:false};

        }
        else{
            return {res:"Unauthorized",msg:"Unauthorized",error:true};
        }
    }
}


module.exports = loginUserControllerLogic;