/**
 * Created by s.charitakis
 */

var wait = require('wait.for');
var moment = require('moment');
var logger = require("../utils/logger");
var conf = require('../config/config');
var mongoose = require('mongoose');

var AddArticlesControllerLogic = function (options) {
    var logMeta = options.logMeta;
    logMeta.facility = "Add Articles Controller";
    this._logMeta = logMeta;
    this.db = options.db;
};

var videoFileTypes =['video/avi','video/mp4','video/quicktime'];

AddArticlesControllerLogic.prototype.startAddArticlesController = function (request, response) {
    var now = moment();
    logger.info("startAddArticlesController: "+moment().diff(now,'millisecond'),this._logMeta);
    var user = request.user;
    var user_id = user.sub;
    var dbMongo = this.db.dbMongo;
    var files = request.files;
    var photos = [];
    var videos = [];
    var description = request.body.description;
    var title = request.body.title;
    var category_id = request.body.category_id;
    var resCreateArticle = null;
    if((title==undefined || title=="")||(description==undefined ||description=="")||(category_id==undefined ||category_id=="")){
        var resJSON ={error:true,msg:"You need to fill in all values except file uploading"}
        return resJSON
    }else {
        //check if valid category
        var categoryIsValid = mongoose.Types.ObjectId.isValid(category_id)
        if(!categoryIsValid){
            var resJSON ={error:true,msg:"The category you entered is not valid"}
            return resJSON
        }
        var findCategory = function (clb) {
            dbMongo.category().findOne({_id: category_id}, clb);
            
        }

        var resCategory = wait.for(findCategory);

        if(!resCategory){
            var resJSON ={error:true,msg:"The category you entered is not valid"}
            return resJSON
        }

        if (files.length) {
            for (var i = 0; i < files.length; i++) {
                if(videoFileTypes.indexOf(files[i].mimetype)>-1){
                    videos.push(conf.servers+'/uploads/'+files[i].filename)
                }
                else{
                    photos.push(conf.servers+'/uploads/'+files[i].filename)
                }

            }
        }


        var newArticle = {
            '_user_id': user_id,
            '_title': title,
            '_description': description,
            '_photos': photos,
            '_category_id': category_id,
            '_videos': videos,
            '_date_created':now.utc().toISOString()
        }

        var createArticle = function (clb) {
            var article = dbMongo.article().create(newArticle, clb);
        }

        resCreateArticle = wait.for(createArticle);
    }


    var resJSON ={res:resCreateArticle,error:null,msg:""}
    return resJSON;
}


module.exports = AddArticlesControllerLogic;