﻿var jwt = require('jsonwebtoken');
var logger = require("../../utils/logger");
var config = require("../../config/config");
var request = require('request');
var async = require('async');
var moment = require('moment');

function auth(req, res, next) {

    var db = req.app.get("app_config");
    var secret =  config.secret;  //req.secret; //this is the preview (dev) secret. we must use the production when we deploy to production. see readme.md

        var authCookie = req.cookies['x-cnnApi-authorization'];

        if (!authCookie) {
            var authHeader = req.header("Authorization");
            if(authHeader){
                var jwtToken = unescape(authHeader).split(' ')[1];
                jwt.verify(jwtToken, secret, function (err, decoded) {
                    if (err) {
                        res.send('You are not authorized');
                        res.end();
                        return
                    }
                    else {

                        decoded.exp = 86400 + Math.floor(new Date() / 1000);// one day 24 hours
                        decoded.iat = Math.floor(new Date() / 1000);
                        var userId = decoded.sub;


                        var updated_token = jwt.sign(decoded, secret);
                        res.append("Authorization",updated_token);
                        req.user = decoded;
                        next();

                    }

                });
            }
            else{
                res.send('You are not authorized');
                res.end();
                return
            }
        }
        else
        {
            var jwtToken = unescape(authCookie).split(' ')[1];
            jwt.verify(jwtToken, secret, function (err, decoded) {
                logger.info("**** jwt =" + JSON.stringify(decoded) + " ****");
                if (err) {
                    res.clearCookie("x-cnnApi-authorization");
                    res.redirect('/');
                    return
                }
                else {

                    decoded.exp = 86400 + Math.floor(new Date() / 1000);// one day 24 hours
                    decoded.iat = Math.floor(new Date() / 1000);
                    var userId = decoded.sub;


                    var updated_token = jwt.sign(decoded, secret);
                    res.cookie("x-cnnApi-authorization", "Bearer " + updated_token,{maxAge: 86400000});
                    req.user = decoded;
                    next();

                }
            });
        }

    
}

module.exports = auth;