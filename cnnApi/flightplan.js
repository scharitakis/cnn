// flightplan.js
var moment = require('moment');
var plan = require('flightplan');

// configuration
plan.target('staging', {
    host: 'lxvd-xweb-99',
    username: 's.charitakis',
    privateKey: '/Users/s.charitakis/.ssh/flightplan1_rsa', // or  password: '*********',
    //password: '',
    agent: process.env.SSH_AUTH_SOCK
});

plan.target('production', [
    {
        host: '192.168.1.17',
        username: 'pi',
        agent: process.env.SSH_AUTH_SOCK
    },
    {
        host: '192.168.1.17',
        username: 'pi',
        agent: process.env.SSH_AUTH_SOCK
    }
]);
var Date = moment().format('D_MM_YYYY_HH_mm') //eg "11_06_2015_18_40"
var tmpDir = 'adserver' + Date

// run commands on localhost
plan.local(function(local) {
    local.log('Starting local');
    local.log('Compress Files');
    //local.exec('svn export https://intqsvn.internetq.corp/svn/minimob-vnext/trunk/adserver/minimob.vnext.adserver.controller/minimob.vnext.adserver.controller deployments/deploy_'+tmpDir)
    //local.exec('tar -zcvf deployments/deploy_'+Date+'.tar.gz deployments/'+tmpDir+'/* --exclude=node_modules --exclude=public/bower_components')
    //local.exec('rm -rf deployments/deploy_'+tmpDir)
    local.exec('tar -zcvf deploy.tar.gz * --exclude=node_modules --exclude=public/bower_components')
    local.log('Copy Compressed File to Destination');
    local.exec('scp s.charitakis@lxvd-xweb-99:tmp/deploy.tar.gz');
    //local.exec('scp s.charitakis@lxvd-xweb-99:tmp/deploy.tar.gz');
});

// run commands on the target's remote hosts
plan.remote(function(remote) {
    remote.log("Starting remote")
    remote.log('Extract Compressed File to adserving Folder');
    remote.exec('tar -zxvf tmp/deploy.tar.gz -C adserving');
    remote.log("==== Start npm install==")
    remote.exec('cd adserving && npm install');
    remote.log("==== Start bower install==");
    remote.exec('cd adserving && bower install');

});