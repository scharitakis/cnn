var repl = require('repl');
var net = require('net');

module.exports = function(app,port) {
    net.createServer(function (socket) {
        var r = repl.start({
            prompt: '[' + process.pid + '] ' + socket.remoteAddress + ':' + socket.remotePort + '> ',
            input: socket,
            output: socket,
            terminal: true,
            useGlobal: false
        })
        r.on('exit', function () {
            socket.end();
        })
        r.context.socket = socket;
        r.context.app = app;
    }).listen(port);
}