﻿/**
 * Created by s.charitakis
 */

var winston = require('winston');
var config = require('../config/config')//.logger[process.env.NODE_ENV || "production"];
winston.emitErrs = true;
var env = process.env.NODE_ENV || 'development';
require('./winston-redis').Redis;
//winston.transports.Kafka = require('winston-kafka-transport');

var debug = new winston.Logger({
    levels: {
        debug: 0
    },
    transports: [
        new (winston.transports.Console)({level: 'debug'})
       /*new winston.transports.Kafka({
            topic: 'my_topic_name',
            connectionString: '192.168.56.101:2181'
        })*/
    ]
});

var info = new winston.Logger({
    levels: {
        info: 1
    },
    transports: [
        new winston.transports.DailyRotateFile({
            level: 'info',
            filename: config.logger.log_dir,
            handleExceptions: true,
            json: true,
            //maxsize: 5242880, //5MB
            //maxFiles: 5,
            colorize: false
        })
        /*new winston.transports.Kafka({
            topic: 'my_topic_name_info',
            connectionString: '192.168.56.101:2181'
        })*/
    ]
});

var warn = new winston.Logger({
    levels: {
        warn: 2
    },
    transports: [
        new winston.transports.DailyRotateFile({
            level: 'warn',
            filename: config.logger.log_dir,
            handleExceptions: true,
            json: true,
            //maxsize: 5242880, //5MB
            //maxFiles: 5,
            colorize: false
        }),
    ]
});

var error = new winston.Logger({
    levels: {
        error: 3
    },
    transports: [
        new winston.transports.DailyRotateFile({
            level: 'error',
            filename: config.logger.log_dir,
            handleExceptions: true,
            json: true,
            //maxsize: 5242880, //5MB
            //maxFiles: 5,
            colorize: false
        })/*,
         new winston.transports.Redis({
         level: 'error',
         host:config.logger.redis.host,
         port:config.logger.redis.port,
         container:config.logger.redis_list,
         length:config.logger.max_queue,
         handleExceptions: true,
         json: true,
         colorize: false
         })*/
    ]
});

var expressError = new winston.Logger({
    levels: {
        error: 3
    },
    transports: [
        new winston.transports.DailyRotateFile({
            level: 'error',
            filename: config.logger.log_dir,
            handleExceptions: true,
            json: true,
            //maxsize: 5242880, //5MB
            //maxFiles: 5,
            colorize: false
        })
    ]
});

var exports = {
    debug: function(msg){
        if(env=="development")
        {
            debug.debug(msg);
        }
    },
    info: function(msg,meta){
        if(env=="development")
        {
            info.info(msg,meta);
        }
    },
    warn: function(msg){
        warn.warn(msg);
    },
    error: function(msg,meta){
        error.error(msg,meta);
    },
    log: function(level,msg){
        var lvl = exports[level];
        lvl(msg);
    },
    expressInfo:info,
    expressError:expressError
};

module.exports = exports;

