var CountryCoo = require('../config/CountryCodesToCoo');
var tz = require("tz-lookup");
var moment = require('moment-timezone');


var timeZ = function(country,coordinates){
    //var country = 'GB'; //uk does not Exist
    var tzwhere;
    if (country !== null) {
        var countryCoo = CountryCoo[country.toLocaleLowerCase()];
        tzwhere = tz(countryCoo[0], countryCoo[1]);
    } else {
        tzwhere = tz(coordinates[0], coordinates[1]);
    }

    var m1 = moment();
    /*console.log(tzwhere)
    console.log(m1.format())*/
    return moment.tz(tzwhere).format('hh:mm');

}



module.exports = timeZ;