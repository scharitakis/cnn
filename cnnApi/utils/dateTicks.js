(function(){

    Date.ticks = function(date)
    {
        this.day = date.getDate();
        this.month = date.getMonth() + 1;
        this.year = date.getFullYear();
        this.hour = date.getHours();
        this.minute = date.getMinutes();
        this.second = date.getSeconds();
        this.ms = date.getMilliseconds();

        this.monthToDays = function(year, month)
        {
            var add = 0;
            var result = 0;
            if((year % 4 == 0) && ((year % 100  != 0) || ((year % 100 == 0) && (year % 400 == 0)))) add++;

            switch(month)
            {
                case 0: return 0;
                case 1: result = 31; break;
                case 2: result = 59; break;
                case 3: result = 90; break;
                case 4: result = 120; break;
                case 5: result = 151; break;
                case 6: result = 181; break;
                case 7: result = 212; break;
                case 8: result = 243; break;
                case 9: result = 273; break;
                case 10: result = 304; break;
                case 11: result = 334; break;
                case 12: result = 365; break;
            }
            if(month > 1) result += add;
            return result;
        }
        this.dateToTicks = function(year, month, day)
        {
            var a = parseInt((year - 1) * 365);
            var b = parseInt((year - 1) / 4);
            var c = parseInt((year - 1) / 100);
            var d = parseInt((a + - c));
            var e = parseInt((year - 1) / 400);
            var f = parseInt(d + e);
            var monthDays = this.monthToDays(year, month - 1);
            var g = parseInt((f + monthDays) + day);
            var h = parseInt(g - 1);
            return h * 864000000000;
        }
        this.timeToTicks = function(hour, minute, second)
        {
            return (((hour * 3600) + minute * 60) + second) * 10000000;
        }

        return this.dateToTicks(this.year, this.month, this.day) + this.timeToTicks(this.hour, this.minute, this.second) + (this.ms * 10000);
    }

})();