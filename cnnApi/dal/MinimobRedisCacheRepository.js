/**
 * Created by s.charitakis on 1/27/2015.
 */


var logger = require("../utils/logger");
var RedisKeyValue = require('./RedisDataStructure/RedisKeyValue');
var RedisHash = require('./RedisDataStructure/RedisHash');
var RedisDBEntityOffers = require('./RedisEntities/RedisDBEntityOffers');

var MinimobRedisCacheRepository = function (options) {
    var port = (options && options.port)?options.port:null;
    var host = (options && options.host)?options.host:null;

    this.port = port;
    this.host = host;
    this.redis = require('redis');
    this.redisClient = this.redis.createClient(this.port, this.host);

    this.redisClient.on('connect', function () {
        logger.info("Redis Cache connection established");
        logger.debug("Redis Cache connection established");
        this.connected = true;
    }.bind(this));

    this.redisClient.on('error', function (err) {
        logger.error('Redis Cache connection error: ' + err);
        this.connectionError = err;
        this.connected = false;
    }.bind(this));

    var _systemPolicy = null;
    this.systemPolicy = function () {
        if (_systemPolicy)
            return _systemPolicy;
        _systemPolicy =  new RedisKeyValue({ client: this.redisClient});
        return _systemPolicy;
    }.bind(this);
    
    var _eligibleDynamicModule = null;
    this.eligibleDynamicModule = function () {
        if (_eligibleDynamicModule)
            return _eligibleDynamicModule;
        _eligibleDynamicModule = new RedisKeyValue({ client: this.redisClient });
        return _eligibleDynamicModule;
    }.bind(this);

    var _filteredCampaings = null;
    this.filteredCampaings = function () {
        if (_filteredCampaings)
            return _filteredCampaings;
        _filteredCampaings =  new RedisKeyValue({ client: this.redisClient});
        return _filteredCampaings;
    }.bind(this);

    var _deviceId = null;
    this.deviceId = function () {
        if (_deviceId)
            return _deviceId;
        _deviceId =  new RedisKeyValue({ client: this.redisClient});
        return _deviceId;
    }.bind(this);

    var _retrieveCashed= null;
    this.retrieveCashed = function () {
        if (_retrieveCashed)
            return _retrieveCashed;
        _retrieveCashed =  new RedisKeyValue({ client: this.redisClient});
        return _retrieveCashed;
    }.bind(this);

    var _wall = null;
    this.wall = function () {
        if (_wall)
            return _wall;
        _wall = new RedisKeyValue({ client: this.redisClient });
        return _wall;
    }.bind(this);

    var _offers = null;
    this.offers = function () {
        if (_offers)
            return _offers;
        _offers = new RedisDBEntityOffers({ client: this.redisClient });
        return _offers;
    }.bind(this);


    var _cacheControl = null;
    this.cacheControl = function () {
        if (_cacheControl)
            return _cacheControl;
        _cacheControl = new RedisHash({ client: this.redisClient,hash:'cacheControl_adserver' });
        return _cacheControl;
    }.bind(this);

}

module.exports = MinimobRedisCacheRepository;