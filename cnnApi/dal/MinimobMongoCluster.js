﻿var MinimobMongoRepository = require('./MinimobMongoRepository.js');

var MinimobMongoCluster = function (options) {
    this.master = (options && options.master) ? options.master:null;
    this.slaves = (options && options.slaves) ? options.slaves:null;
    this.localCache = (options && options.localCache) ? options.localCache:null;
    this.app = (options && options.app) ? options.app:null;


    if (this.master){
        this.masterRepository = new MinimobMongoRepository({
            connectionString: this.master,
            master:true,
            localCache:this.localCache,
            app:this.app
        });
        this.masterRepository.on("reconnected",this.reconnectSlaves.bind(this));
    }

    if (this.slaves && this.slaves.length) {
        this.slaveRepositories = [];
        
        for (var i = 0; i < this.slaves.length; i++) {
            this.slaveRepositories.push(new MinimobMongoRepository({
                connectionString: this.slaves[i],
                master:false,
                localCache:this.localCache,
                app:this.app
            }));
        }
    }
    
}


MinimobMongoCluster.prototype.reconnectSlaves = function () {
        this.slaveRepositories.length = 0;
        for (var i = 0; i < this.slaves.length; i++) {
            this.slaveRepositories.push(new MinimobMongoRepository({
                connectionString: this.slaves[i],
                mongoCluster:this
            }));
        }


}

MinimobMongoCluster.prototype.randomSlaveRepository = function () {
    if (this.slaveRepositories && this.slaveRepositories.length) {
        var count = this.slaveRepositories.length;
        
        if (count > 0) {
            var random = parseInt(Math.random() * 1000) % count;
            return this.slaveRepositories[random];
        }
    }

    return null;
}

MinimobMongoCluster.prototype.randomMongoSlaveConnectionString = function () {
    if (this.slaveRepositories && this.slaveRepositories.length) {
        var count = this.slaveRepositories.length;
        
        if (count > 0) {
            var random = parseInt(Math.random() * 1000) % count;
            return this.slaveRepositories[random].connectionString;
        }
    }
    
    return null;
}

module.exports = MinimobMongoCluster;