/**
 * Created by s.charitakis on 1/27/2015.
 */


var logger = require("../utils/logger");
var RedisDBEntityImei = require('./RedisEntities/RedisDBEntityImei');

var MinimobRedisShardRepository = function (options) {
    var port = (options && options.port)?options.port:null;
    var host = (options && options.host)?options.host:null;

    this.port = port;
    this.host = host;
    this.redis = require('redis');
    this.redisClient = this.redis.createClient(this.port, this.host);

    this.redisClient.on('connect', function () {
        logger.info("Redis Shard connection established");
        logger.debug("Redis Shard connection established");
        this.connected = true;
    }.bind(this));

    this.redisClient.on('error', function (err) {
        logger.error('connection error: ' + err);
        this.connectionError = err;
        this.connected = false;
    }.bind(this));


    var _imei = null;
    this.imei = function () {
        if (_imei)
            return _imei;
        _imei = new RedisDBEntityImei({ client: this.redisClient });
        return _imei;
    }.bind(this);


}


module.exports = MinimobRedisShardRepository

