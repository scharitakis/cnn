/**
 * Created by s.charitakis on 3/26/2015.
 */

var wait = require('wait.for');
var moment = require('moment');


var Optimization = function (options) {
    this.db = (options && options.db)?options.db:null;
    this.optPolicy = null;
}

Optimization.prototype.load = function (policyid) //LOAD
{
    var db = this.db;
    var getOptimization = function (clb) {
        db.dbSite.optimization().get('opt'+policyid, clb)
    }
    
    this.optPolicy = JSON.parse(wait.for(getOptimization));
    
    //# Correct ratios if fractional
    if ((this.optPolicy._target_ratio > 0) && (this.optPolicy._target_ratio < 1)) {
        this.optPolicy._target_ratio = parseInt(this.optPolicy._target_ratio * 100 + 0.5);
    }
    
    //# Correct offset
    while (!((this.optPolicy._target_ratio + this.optPolicy._ratio_offset < 100) && (this.optPolicy._target_ratio - this.optPolicy._ratio_offset > 0))) {
        this.optPolicy._ratio_offset -= 1; //TODO : error here
    }
    
    return this;

}

Optimization.prototype.privNewId = function () {
    var db = this.db;
    var id = "";
    do {
        var getIncr = function (clb) {
            db.dbSite.incrOptPolicyId().incr('incr_opt_policy_id', clb)
        }
        id = wait.for(getIncr);
        
        var checkExists = function () {
            db.dbSite.incrOptPolicyId().exists('opt' + id, clb)
        }
        var exists = wait.for(checkExists);

    } while(exists)
    
    return id

}

Optimization.prototype.new = function (options) {
    
    var db = this.db;
    var optObj = {}
    if (options && options.ownerid && options.ownerid > 0) {
        optObj['_owner_id'] = options.ownerid
    }
    if (options && options.name) {
        optObj['_name'] = options.name
    }
    
    if (options && options.step && options.step >= 50) {
        optObj['_step'] = options.step + 0
    }
    
    if (options && options.ratio && options.ratio <= 99 && options.ratio >= 1) {
        optObj['_target_ratio'] = options.ratio + 0.5
    }
    
    if (options && options.third_party) {
        optObj['_third_party'] = options.third_party;
    }
    
    if (options && options.offset && options.offset <= 30 && options.offset >= 0) {
        optObj['_offset'] = options.offset + 0.5;
        while (!((optObj._target_ratio + optObj.optPolicy._ratio_offset < 100) && (optObj._target_ratio - optObj._ratio_offset > 0))) {
            optObj._ratio_offset -= 1;
        }
    }
    optObj._modified = moment().unix();
    optObj._policy_id = this.privNewId();
    optObj._object_id = 'opt' + optObj._policy_id;
    optObj._class = "Minimob::Application::Optimization";
    optObj._version = "1.00";
    optObj._applications = {};
    
    var data = JSON.stringify(optObj)
    var saveOptimization = function (clb) {
        db.dbSite.optimization().set(optObj._object_id, data, clb)
    }
    
    var res = wait.for(saveOptimization);
    
    this.optPolicy = optObj;
    
    return this;

}


Optimization.prototype.getIsOptimized = function (appid) {
    var db = this.db
    
    var optPolicy = this.optPolicy;
    if (moment().unix() > optPolicy._applications[appid]._ratio_expires || 
        optPolicy._applications[appid]._current_ratio >= 100 ||
        optPolicy._applications[appid]._current_ratio < 0) {
        var offset = this.privGetNewOffset(optPolicy._ratio_offset);
        optPolicy._applications[appid]._current_ratio = optPolicy._target_ratio + offset;
        optPolicy._applications[appid]._ratio_expires = this.prinGetNewExpire()
        optPolicy._modified = 0;
    } //TODO: Error
    
    // # get total installations (method increments by 1)
    var incrTotalInstalls = function (clb) {
        db.dbSite.optimization().incr('counter_inst_' + appid, clb)
    }
    
    var total_installs = wait.for(incrTotalInstalls);
    
    // # get the level of the discount ratio to apply given the total installations and the step 
    var ratio_level = this.privOptimizationLevel({ installs: total_installs, step: optPolicy._step });
    
    // # get the optimization probability
    var probability = optPolicy._applications[appid]._current_ratio * ratio_level / 100;
    
    // # policy decision
    var optimized = Math.random(1) < probability?1:0;
    
    // # set the hidden counter if optimized is true 
    if (optimized) {
        var incrHiddenCounter = function (clb) {
            db.dbSite.optimization().incr('counter_inst_' + appid + '_hidden', clb)
        }
        wait.for(incrHiddenCounter);
    }
    

}

Optimization.prototype.privGetNewOffset = function (offset) {
    return parseInt(Math.random() * offset * 2) - offset;
}

Optimization.prototype.prinGetNewExpire = function () {
    // # We expire between 1 and 4 hours
    return (parseInt(Math.random() * 4) + 1) * 3600 + moment().unix();
}

Optimization.prototype.privOptimizationLevel = function (options) {
    var installs = options.installs;
    var step = options.step;
    
    if (installs < step) {
        return 0;
    }
    
    if (installs > Math.pow(2, 7) * step) {
        return 1;
    }
    
    var stage = options.state? options.stage : 0;
    
    var lower_boundary = Math.pow(2, stage) * stage;
    stage += 1;
    var upper_boundary = Math.pow(2, stage) * stage;
    if (installs >= lower_boundary && installs < upper_boundary) {
        return Math.pow(2, stage - 8);
    }
    // # recurse
    return this.privOptimizationLevel({ installs: installs, stage: stage, step: step });
}


module.exports = Optimization;