/**
 * Created by s.charitakis
 */

var bigNumber = require("../../utils/bigNumber");

var EXPIRES	= 592000;

/*
var readonlyProperties = [
    "registration_id",
    "registration_date",
    "is_registered",
    "is_optimized",
    "all_campaign_delivered"
]

var expiresProperties = [
    "appid",
    "deviceid",
    "other_campaign_delivered"
]
*/
var properties = [
    'deviceid',
    'disabled',
    'androidsdk',
    'phonemodel',
    'manufacturer',
    'osversion',
    'androidid',
    'adid',
    'locale',
    'sdkversion',
    'networkoperator',
    'carrier',
    'connectiontype',
    'networktype',
    'isp',
    'ipcountry',
    'country',
    'longitude',
    'latitude',
    'last_boot',
    'last_dev_push_click',
    'dev_push_break',
    'dev_icon_break'
]

/* OLD
var properties = [
    "phonemodel",
    "androidsdk",
    "manufacturer",
    "androidid",
    "adid",
    "locale",
    "sdkversion",
    "carrier",
    "osversion",
    "networkoperator",
    "connectiontype",
    "networktype",
    "isp",
    "ipcountry",
    "country",
    "longitude",
    "latitude",
    "last_boot",
    "sdk" //TODO: might not need it
]
*/



var sdkProperties = [
    'registration_id',
    'registration_date',
    'is_registered',
    'package_name',
    'appid',
    'is_optimized',
    'is_active',
    'last_activity',
    'last_applaunch',
    'last_install',
    'last_push_click',
    'next_polling',
    'sdk2_last_delivered',
    'dynamic_module_delivered',
    'permissions',
    'sdk2_status',
    'play_status',
    'installer_package',
    'inapp_break',
    'push_break',
    'icon_break',
    'wait_disclaimer',
    'campaign_delivered',
    'other_campaign_delivered'	// # Ads Delivered by other Apps. Should not be seriliazed, only in memory use
];



/* OLD
var sdkProperties = [
    "last_activity",
    "sdk2_last_delivered",
    "next_polling",
    "registration_id",
    "wait_disclaimer",
    "last_install",
    "campaign_delivered",
    "last_applaunch",
    "is_optimized",
    "play_status",
    "is_registered",
    "permissions",
    "package_name",
    "is_active",
    "inapp_break",
    "registration_date",
    "installer_package",
    "sdk2_status",
    "disabled" //TODO: don't know if exists
];*/




var RedisDBEntityImei = function (options){
    this.redisClient = (options && options.client ? options.client : null);
}

//TODO: needs testing 
RedisDBEntityImei.prototype.set = function(key,fields,callback)
{
    var _properties = Object.keys(fields);
    if(_properties.length){
        var results = {};
        var multi = this.redisClient.multi();
        for (var i = 0; i < _properties.length; i++) {
            var property = _properties[i];
            multi.set("urn:m:d:" + key + ":" + property, fields[property], function (err, reply) {
            });
        }
        multi.exec(function (err, replies) {
            if (!err) {
                for (var i = 0; i < _properties.length; i++) {
                    var property = _properties[i];
                    results[property] = replies[i];
                }
                return callback(null, results);
            }
            else {
                return callback(err, null);
            }
        });
    }else{
        return callback("Cannot set: no values", null);
    }
}

RedisDBEntityImei.prototype.get = function(key,fields,callback)
{
    var _properties = !(fields || fields.length) ? properties : fields
    var results = {};
    var multi = this.redisClient.multi();
    for (var i = 0; i < _properties.length; i++) {
        var property = _properties[i];
        multi.get("urn:m:d:"+key+":"+property, function (err, reply) {

        });
    }
    multi.exec(function (err, replies) {
        if (!err) {
            for (var i = 0; i < _properties.length; i++) {
                var property = _properties[i];
                results[property] = JSON.parse(replies[i]);
            }
            return callback(null, results);
        }
        else {
            return callback(err, null);
        }
    });
}

RedisDBEntityImei.prototype.getSdk = function(appid,key,fields,callback)
{
    var sdkKeyPrefix = "sdk:000000" + bigNumber(appid).toString(16)+":";
    var _properties = !(fields || fields.length) ? properties : fields
    var results = {};
    var multi = this.redisClient.multi();
    for (var i = 0; i < _properties.length; i++) {
        var property = sdkKeyPrefix+_properties[i];
        multi.get("urn:m:d:"+key+":"+property, function (err, reply) {
        });
    }
    multi.exec(function (err, replies) {
        if (!err) {
            for (var i = 0; i < _properties.length; i++) {
                var property = _properties[i];
                results[property] = JSON.parse(replies[i]);
            }
            return callback(null, results);
        }
        else {
            return callback(err, null);
        }
    });
}


RedisDBEntityImei.prototype.setSdk = function(appid,key,fields,callback)
{
    var sdkKeyPrefix = "sdk:000000" + bigNumber(appid).toString(16)+":";
    var _properties = fields && fields.length ? fields : properties;
    var results = {};
    var multi = this.redisClient.multi();
    for (var i = 0; i < _properties.length; i++) {
        var propertyKey = Object.keys(_properties[i])[0]
        var property = sdkKeyPrefix + propertyKey;
        var value = _properties[i][propertyKey]
        multi.set("urn:m:d:" + key + ":" + property,value, function (err, reply) {

        });
    }
    multi.exec(function (err, replies) {
        if (!err) {
            for (var i = 0; i < _properties.length; i++) {
                var propertyKey = Object.keys(_properties[i])[0]
                results[propertyKey] = replies[i];
            }
            return callback(null, results);
        }
        else {
            return callback(err, null);
        }
    });
}


RedisDBEntityImei.prototype.getAll = function(appid,key,callback)
{
    var _properties =  properties;
    var sdkKeyPrefix = "sdk:000000" + bigNumber(appid).toString(16)+":";
    var _sdkProperties = sdkProperties;
    var _sdkPropertiesKeys = []
    for(var i=0; i<_sdkProperties.length; i++){
        _sdkPropertiesKeys.push(sdkKeyPrefix+_sdkProperties[i])
    }
    var propertyKeys = _properties.concat(_sdkPropertiesKeys);
    var allProperies = _properties.concat(_sdkProperties);
    var results = {};
    var multi = this.redisClient.multi();
    for (var i = 0; i < propertyKeys.length; i++) {
        var property = propertyKeys[i];
        multi.get("urn:m:d:"+key+":"+property, function (err, reply) {});
    }
    multi.exec(function (err, replies) {
        if (!err) {
            for (var i = 0; i < allProperies.length; i++) {
                var property = allProperies[i];
                results[property] = replies[i];
            }
            return callback(null, results);
        }
        else {
            return callback(err, null);
        }
    });
}


//TODO: Test it
RedisDBEntityImei.prototype.setAll = function(appid,key,values,callback)
{
    var data = {};
    var _properties = properties;
    for(var i=0;i<properties.length;i++)
    {
        if(values[properties[i]]) {
            data[properties[i]] = values[properties[i]];
        }
    }
    var sdkKeyPrefix = "sdk:000000" + bigNumber(appid).toString(16)+":";
    var _sdkProperties = sdkProperties;
    for(var i=0; i<_sdkProperties.length; i++){
        if(values[_sdkProperties[i]]) {
            data[sdkKeyPrefix + _sdkProperties.length[i]] = values[_sdkProperties[i]];
        }
    }
    var propertyKeys = Object.keys(data);
    var results = {};
    var multi = this.redisClient.multi();
    for (var i = 0; i < propertyKeys.length; i++) {
        var property = propertyKeys[i];
        multi.set("urn:m:d:"+key+":"+property,data[property], function (err, reply) {});
    }
    multi.exec(function (err, replies) {
        if (!err) {
            for (var i = 0; i < propertyKeys.length; i++) {
                var property = propertyKeys[i];
                results[property] = replies[i];
            }
            return callback(null, results);
        }
        else {
            return callback(err, null);
        }
    });
}

RedisDBEntityImei.prototype.setExAll = function(appid,key,values,callback)
{
    var data = {};
    var _properties = properties;
    for(var i=0;i<properties.length;i++)
    {
        if(values[properties[i]]) {
            data[properties[i]] = values[properties[i]];
        }
    }
    var sdkKeyPrefix = "sdk:000000" + bigNumber(appid).toString(16)+":";
    var _sdkProperties = sdkProperties;
    for(var i=0; i<_sdkProperties.length; i++){
        if(values[_sdkProperties[i]]) {
            data[sdkKeyPrefix + _sdkProperties[i]] = values[_sdkProperties[i]];
        }
    }
    var propertyKeys = Object.keys(data);
    var results = {};
    var multi = this.redisClient.multi();
    for (var i = 0; i < propertyKeys.length; i++) {
        var property = propertyKeys[i];
        multi.setex("urn:m:d:"+key+":"+property,EXPIRES,data[property], function (err, reply) {});
    }
    multi.exec(function (err, replies) {
        if (!err) {
            for (var i = 0; i < propertyKeys.length; i++) {
                var property = propertyKeys[i];
                results[property] = replies[i];
            }
            return callback(null, results);
        }
        else {
            return callback(err, null);
        }
    });
}

RedisDBEntityImei.prototype.getOtherCampaigns = function(key,callback)
{
    var rClient = this.redisClient

    var results = {};   //{"000453440":{"urn:m:d:xxxxx:sdk:xxxxxx:property": THE_RESULT}}

    rClient.smembers("urn:m:d:"+key+":sdk",function(err,apps){
        if(err)
        {
          return callback(err,null)
        }
        var multi = rClient.multi();
        for(var i=0;i<apps.length;i++)
        {
            results[apps[i]] = {};
            var sdkKeyPrefix = "sdk:"+apps[i]+":";
            for(var d=0;d<sdkProperties.length;d++) {
                var appKey = "urn:m:d:" + key + ":" + sdkKeyPrefix+sdkProperties[d]
                multi.get(appKey, function (err, reply) {});
            }
        }
        multi.exec(function (err, replies) {
            if (!err) {
                for (var i = 0; i < replies.length; i++) {
                    var reply = JSON.parse(replies[i]);
                    var app_indx = parseInt(i / sdkProperties.length);
                    var prop_indx = i % sdkProperties.length;
                    results[apps[app_indx]][sdkProperties[prop_indx]] = reply;
                }

                return callback(null, results);
            }
            else {
                return callback(err, null);
            }
        });


    })


}


module.exports = RedisDBEntityImei;