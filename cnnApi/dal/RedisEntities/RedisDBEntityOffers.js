/**
 * Created by s.charitakis
 */
var RedisDBEntityOffers = function(options){
    //this.max_queue = (options && options.max_queue ? options.max_queue : null);
    // this.list = (options && options.list ? options.list : null);
    this.redisClient = (options && options.client ? options.client : null);

}

RedisDBEntityOffers.prototype.llen = function(list,callback)
{
    this.redisClient.llen(list, function (err, reply) {
        return callback(err, reply);
    });
}


RedisDBEntityOffers.prototype.lpush = function(list,value,callback)
{

    this.redisClient.lpush(list, value, function (err, reply) {
        return callback(err, reply);
    });

}

RedisDBEntityOffers.prototype.rpush = function(list,value,callback)
{
    var _this = this
    this.llen(function(err,res){
        if(err){
            callback(err, null);
        }
        if(res<_this.max_queue) {
            _this.redisClient.rpush(list, value,  function (err, reply) {
                return callback(err, reply);
            });
        }
    })
}

RedisDBEntityOffers.prototype.lrange = function(list,start,end,callback)
{

    this.redisClient.lrange(list, start,end, function (err, reply) {
        return callback(err, reply);

    });

}

RedisDBEntityOffers.prototype.getOffers = function(list,start,end,callback)
{
    var multi = this.redisClient.multi();
    multi.llen(list,function (err, reply) { });
    multi.lrange(list, start,end, function (err, reply) { });
    multi.exec(function (err, replies) {
        return callback(err, replies);
    });
}

RedisDBEntityOffers.prototype.rpushMultiEx = function(list,value,callback)
{
    var arr = []
    for(var i=0;i<value.length;i++){
        arr.push(JSON.stringify(value[i]));
    }
    var client = this.redisClient.multi();
    client.rpush.apply(client, [list].concat(arr));
    client.expire(list, 1200, function (err, reply){});
    client.exec(function (err, replies) {
        if (!err) {
            return callback(null,true);
        }
        else {
            return callback(err, null);
        }
    });

}

RedisDBEntityOffers.prototype.getCachedMultiOffers = function(list,key,start,end,callback)
{
    var multi = this.redisClient.multi();
    multi.lrange(list, start,end, function (err, reply) { });
    multi.get(key,function(err,reply){});

    multi.exec(function (err, replies) {
        if (!err) {
            return callback(null,replies);
        }
        else {
            return callback(err, null);
        }
    });
}

RedisDBEntityOffers.prototype.getCachedMultiUserOffers = function(list,start,end,callback)
{
    var multi = this.redisClient.multi();
    multi.lrange(list, start,end, function (err, reply) { });
    multi.llen(list,function(err,reply){});

    multi.exec(function (err, replies) {
        if (!err) {
            return callback(null,replies);
        }
        else {
            return callback(err, null);
        }
    });
}


RedisDBEntityOffers.prototype.lpop = function(list,callback)
{
    this.redisClient.set(list, function (err, reply) {
        return callback(err, reply);
    });
}

RedisDBEntityOffers.prototype.rpop = function(list,callback)
{
    this.redisClient.rpop(list, function (err, reply) {
        return callback(err, reply);
    });
}


module.exports = RedisDBEntityOffers;