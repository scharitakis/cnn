/**
 * Created by s.charitakis
 */
var moment = require('moment');

var event = {
        eventtm:null,
        trunc_hour:null,
        trunc_day:null,
        trunc_week:null,
        trunc_month:null,
        trunc_year:null,
        appid:null,
        packageownerid:null,
        registrationid:null,
        country:null,
        osversion:null,
        networktype:null,
        connectiontype:null,
        version:null,
        androidid:null,
        adid:null,
        imeisource:null,
        imei:null,
        networkoperator:null,
        carrier:null,
        allinstallsources:null,
        playauthorized:null,
        playservicestatus:null,
        installer_isplay:null,
        remote_address:null,
        manufacturer:null,
        n_id:null,
        n_message:null,
        n_package:null,
        n_correlatorid:null,
        n_command:null,
        n_success:null,
        n_step:null,
        n_campaignid:null,
        n_creativeid:null,
        n_offerid:null,
        n_advertiserid:null
}

var RedisDBEventNotification= function (options){
    this.redisClient = (options && options.client ? options.client : null);
    this.list = (options && options.list ? options.list : null);
}

RedisDBEventNotification.prototype.store = function(request, imeiData, appData, callback)
{
    var now = new moment();
    var newEvent = Object.create(event);
    
    newEvent.eventtm = now.unix();
    newEvent.trunc_hour = now.clone().startOf('hour').unix();
    newEvent.trunc_day = now.clone().startOf('day').unix();
    newEvent.trunc_week = now.clone().startOf('week').unix();
    newEvent.trunc_month = now.clone().startOf('month').unix();
    newEvent.trunc_year = now.clone().startOf('year').unix();
    newEvent.appid = request.appid   || 0;
    newEvent.packageownerid = appData._owner_id || 0;
    newEvent.registrationid = imeiData.registration_id  || -1;
    newEvent.country = request.country   || 'XX';
    newEvent.osversion = request.osversion   || '0.0';
    newEvent.networktype = request.networktype_id   || 0;
    newEvent.connectiontype = request.connectiontypeId()   || 0;
    newEvent.version = request.version   || '0.0';
    newEvent.androidid = request.androidid;
    newEvent.adid = request.adid;
    newEvent.imeisource = request.idsource_id   || 0;
    newEvent.imei = request.deviceflexid   || 'X';
    newEvent.networkoperator = request.networkoperator;
    newEvent.carrier = request.carrier   || '0';
    newEvent.allinstallsources = request.hasAllInstallSources() ? 1 : 0;
    newEvent.playauthorized = request.hasPlayAuthorized() ? 1 : 0;
    newEvent.playservicestatus = request.playServicesStatusId();
    newEvent.installer_isplay = request.getInstallerIsPlay() ? 1 : 0;
    newEvent.remote_address = request.remote_address   || '0';
    newEvent.manufacturer = request.manufacturer   || '0'; 
    newEvent.n_id = request.getNotificationIntId()   || 0;
    newEvent.n_message = request.notification_message;
    newEvent.n_package = request.notification_package;
    newEvent.n_correlatorid = request.notification_creativeid   || '';
    newEvent.n_command = request.notification_command_id;
    newEvent.n_success = request.notification_success ? 1 : 0;
    newEvent.n_step = request.notification_step == 'after' ? 1 : 0;
    newEvent.n_campaignid = request.correlator ? request.correlator.campaignid : 0;
    newEvent.n_creativeid = request.correlator ? request.correlator.creativeid : 0;
    newEvent.n_offerid = request.correlator ? request.correlator.offerid : 0;
    newEvent.n_advertiserid = request.correlator ? request.correlator.advertiserid : 0;

    this.redisClient.rpush(newEvent,  function (err, reply) {
        return callback(err, reply);
    });

}

RedisDBEventNotification.prototype.llen = function(callback)
{
    this.redisClient.llen(this.list, function (err, reply) {
        return callback(err, reply);
    });
}


module.exports = RedisDBEventNotification;