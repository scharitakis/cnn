/**
 * Created by s.charitakis
 */
var moment = require('moment');
var constants = require('../../config/constants');

var event = {
    eventtm:null,
    trunc_hour:null,
    trunc_day:null,
    trunc_week:null,
    trunc_month:null,
    trunc_year:null,
    imei:null,
    registrationid:null,
    appid:null,
    publisherid:null,
    countrycode:null,
    optimized:null,
    adchannelReq:null,
    adchannelResp:null,
    campaignid:null,
    creativeid:null,
    offerid:null,
    advertiserid:null,
    wallslot:null,
    wallfilled:null,
    platform:null,
    version:null,
    manufacturer:null,
    appstore:null,
    pollid:null,
    trayAdv:null,
    traybannerAdv:null,
    dialogueoutAdv:null,
    dialogueinAdv:null,
    iconAdv:null,
    appwallAdv:null,
    bannerAdv:null,
    sdkversion:null,
    anyAdv:null,
    serverid:null
}

var RedisDBEventFillRate= function (options){
    this.redisClient = (options && options.client ? options.client : null);
    this.list = (options && options.list ? options.list : null);
}

RedisDBEventFillRate.prototype.store = function(value,callback)
{
    var new_event = Object.create(event);
    var new_event_keys = Object.keys(value);
    for (var i = 0; i < new_event_keys.length; i++) {
        if (value[new_event_keys[i]] != undefined) {
            new_event[new_event_keys[i]] = value[new_event_keys[i]];
        }
    }
    var timestamp_moment = new moment(value.timestamp);
    new_event.trunc_hour = timestamp_moment.clone().startOf('hour').valueOf();
    new_event.trunc_day = timestamp_moment.clone().startOf('day').valueOf();
    new_event.trunc_week = timestamp_moment.clone().startOf('week').valueOf();
    new_event.trunc_month = timestamp_moment.clone().startOf('month').valueOf();
    new_event.trunc_year = timestamp_moment.clone().startOf('year').valueOf();
  
    //# AnyAdv set to true if package allows Advertiser Ads for any of the requested AdChannels
    
    if (new_event.adchannelReq == constants.ADFORMAT_ALL_IN_APP && (new_event.bannerAdv || new_event.diologueinAdv || new_event.appwallAdv)) {
        new_event.anyAdv = 1;
    } else if (new_event.adchannelReq == constants.ADFORMAT_DIALOGUE_IN_APP && new_event.dialogueinAdv) {
        new_event.anyAdv = 1;
    } else if ((new_event.adchannelReq == constants.ADFORMAT_INTERSTITIAL || new_event.adchannelReq == constants.ADFORMAT_NON_INTERSTITIAL) && new_event.bannerAdv) {
        new_event.anyAdv = 1;
    } else if (new_event.adchannelReq == constants.ADFORMAT_APPWALL && new_event.appwallAdv) {
        new_event.anyAdv = 1;
    } else if (new_event.adchannelReq == 0 && (new_event.trayAdv || new_event.traybannerAdv || new_event.dialogueoutAdv || new_event.iconAdv)) {
        new_event.anyAdv = 1;
    } else {
        new_event.anyAdv = 0;
    }
    
    new_event.serverid = value.hostname;
    
    this.redisClient.rpush(this.list, new_event, function (err, reply) {
        return callback(err, reply);
    });

}

RedisDBEventFillRate.prototype.llen = function(callback)
{
    this.redisClient.llen(this.list, function (err, reply) {
        return callback(err, reply);
    });
}


module.exports = RedisDBEventFillRate;