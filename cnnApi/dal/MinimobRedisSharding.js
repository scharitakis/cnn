/**
 * Created by s.charitakis on 1/27/2015.
 */

var MinimobRedisRepository = require('./MinimobRedisShardRepository');

var MinimobRedisSharding = function(options){
    this.shards = options || null;
    this.shards_map = {}
    this.clients_map = {}

    if(this.shards) {
        for (var i = 0; i<this.shards.length; i++)
        {
            this.clients_map[i] = (new MinimobRedisRepository(this.shards[i]));
        }

        for (var i = 0; i <= 1023; i++) {
            var shard = parseInt(i / (1024 / this.shards.length));
            this.shards_map[i] = this.clients_map[shard];
        }
    }

}

MinimobRedisSharding.prototype.shard = function(imei)
{
    var dec = parseInt(imei.substring(24, imei.length).toLowerCase(),16);
    var shard =  parseInt(dec/Math.pow(2, 22));
    return this.shards_map[shard];
}

module.exports = MinimobRedisSharding