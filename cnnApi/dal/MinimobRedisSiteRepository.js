﻿/**
 * Created by s.charitakis on 1/27/2015.
 */

var logger = require("../utils/logger");
var RedisKeyValue = require('./RedisDataStructure/RedisKeyValue');
var RedisHash = require('./RedisDataStructure/RedisHash');
var RedisSet = require('./RedisDataStructure/RedisSet');
var RedisSortedSets = require('./RedisDataStructure/RedisSortedSets');
var RedisList = require('./RedisDataStructure/RedisList');

var MinimobRedisSiteRepository = function (options) {
    var port = (options && options.port)?options.port:null;
    var host = (options && options.host)?options.host:null;
    
    this.port = port;
    this.host = host;
    this.redis = require('redis');
    this.redisClient = this.redis.createClient(this.port, this.host);

    this.redisClient.on('connect', function () {
        logger.info("Redis Site connection established");
        logger.debug("Redis Site connection established");
        this.connected = true;
    }.bind(this));
    
    this.redisClient.on('error', function (err) {
        logger.error('connection error: ' + err);
        this.connectionError = err;
        this.connected = false;
    }.bind(this));


    // ================================= KEY VALUE

    var _users = null;
    this.users = function () {
        if (_users)
            return _users;
        _users =  new RedisKeyValue({ client: this.redisClient});
        return _users;
    }.bind(this);

    var _roles = null;
    this.roles = function () {
        if (_roles)
            return _roles;
        _roles =  new RedisKeyValue({ client: this.redisClient});
        return _roles;
    }.bind(this);



    var _applications = null;
    this.applications = function () {
        if (_applications)
            return _applications;
        _applications =  new RedisKeyValue({ client: this.redisClient});
        return _applications;
    }.bind(this);
    
    
    var _manufacturer = null;
    this.manufacturer = function () {
        if (_manufacturer)
            return _manufacturer;
        _manufacturer = new RedisKeyValue({ client: this.redisClient });
        return _manufacturer;
    }.bind(this);
    
    var _phonemodel = null;
    this.phonemodel = function () {
        if (_phonemodel)
            return _phonemodel;
        _phonemodel = new RedisKeyValue({ client: this.redisClient });
        return _phonemodel;
    }.bind(this);


    var _incrThrottling = null;
    this.incrThrottling = function () {
        if (_incrThrottling)
            return _incrThrottling;
        _incrThrottling = new RedisKeyValue({ client: this.redisClient });
        return _incrThrottling;
    }.bind(this);


    var _optimization = null;
    this.optimization = function () {
        if (_optimization)
            return _optimization;
        _optimization = new RedisKeyValue({ client: this.redisClient });
        return _optimization;
    }.bind(this);

    var _incrOptPolicyId = null;
    this.incrOptPolicyId = function () {
        if (_incrOptPolicyId)
            return _incrOptPolicyId;
        _incrOptPolicyId = new RedisKeyValue({ client: this.redisClient });
        return _incrOptPolicyId;
    }.bind(this);

    var _appWall = null;
    this.appWall = function () {
        if (_appWall)
            return _incrOptPolicyId;
        _appWall = new RedisKeyValue({ client: this.redisClient });
        return _appWall;
    }.bind(this);

    // ===============  Hash

    var _hashUserToUserId = null;
    this.hashUserToUserId = function () {
        if (_hashUserToUserId)
            return _hashUserToUserId;
        _hashUserToUserId =  new RedisHash({
            client: this.redisClient,
            hash: "hash_user_touserid"});
        return _hashUserToUserId;
    }.bind(this);

    var _hashUserIdToUser = null;
    this.hashUserIdToUser = function () {
        if (_hashUserIdToUser)
            return _hashUserIdToUser;
        _hashUserIdToUser =  new RedisHash({
            client: this.redisClient,
            hash: "hash_userid_touser"});
        return _hashUserIdToUser;
    }.bind(this);

    var _hashUserToApiKey = null;
    this.hashUserToApiKey = function () {
        if (_hashUserToApiKey)
            return _hashUserToApiKey;
        _hashUserToApiKey =  new RedisHash({
            client: this.redisClient,
            hash: "hash_userid_toapikey"});
        return _hashUserToApiKey;
    }.bind(this);

    var _hashApikeyToUserid = null;
    this.hashApikeyToUserid = function () {
        if (_hashApikeyToUserid)
            return _hashApikeyToUserid;
        _hashApikeyToUserid =  new RedisHash({
            client: this.redisClient,
            hash: "hash_apikey_touserid"});
        return _hashApikeyToUserid;
    }.bind(this);

    var _hashUserIdToHiddenApp = null;
    this.hashUserIdToHiddenApp = function () {
        if (_hashUserIdToHiddenApp)
            return _hashUserIdToHiddenApp;
        _hashUserIdToHiddenApp =  new RedisHash({
            client: this.redisClient,
            hash: "hash_userid_to_hidden_appid"});
        return _hashUserIdToHiddenApp;
    }.bind(this);

    var _hashAppidToPackagename = null;
    this.hashAppidToPackagename = function () {
        if (_hashAppidToPackagename)
            return _hashAppidToPackagename;
        _hashAppidToPackagename =  new RedisHash({
            client: this.redisClient,
            hash: "hash_appid_topackagename"});
        return _hashAppidToPackagename;
    }.bind(this);

    var _hashPackagenameToAppid = null;
    this.hashPackagenameToAppid = function () {
        if (_hashPackagenameToAppid)
            return _hashPackagenameToAppid;
        _hashPackagenameToAppid =  new RedisHash({
            client: this.redisClient,
            hash: "hash_packagename_toappid"});
        return _hashPackagenameToAppid;
    }.bind(this);

    var _hashAppidToAppname = null;
    this.hashAppidToAppname = function () {
        if (_hashAppidToAppname)
            return _hashAppidToAppname;
        _hashAppidToAppname =  new RedisHash({
            client: this.redisClient,
            hash: "hash_appid_toappname"});
        return _hashAppidToAppname;
    }.bind(this);

    var _hashAppidToDateAdded = null;
    this.hashAppidToDateAdded = function () {
        if (_hashAppidToDateAdded)
            return _hashAppidToDateAdded;
        _hashAppidToDateAdded =  new RedisHash({
            client: this.redisClient,
            hash: "hash_appid_todate_added"});
        return _hashAppidToDateAdded;
    }.bind(this);

    var _hashActiveApplicationsByPublisher = null;
    this.hashActiveApplicationsByPublisher = function () {
        if (_hashActiveApplicationsByPublisher)
            return _hashActiveApplicationsByPublisher;
        _hashActiveApplicationsByPublisher =  new RedisHash({
            client: this.redisClient,
            hash: "hash_active_applications_by_publisher"});
        return _hashActiveApplicationsByPublisher;
    }.bind(this);

    var _hashOfferQualityInfo = null;
    this.hashOfferQualityInfo = function () {
        if (_hashOfferQualityInfo)
            return _hashOfferQualityInfo;
        _hashOfferQualityInfo =  new RedisHash({
            client: this.redisClient,
            hash: "hash_offer_quality_info"});
        return _hashOfferQualityInfo;
    }.bind(this);

    var _hash_imsi_isocn = null;
    this.hash_imsi_isocn = function () {
        if (_hash_imsi_isocn)
            return _hash_imsi_isocn;
        _hash_imsi_isocn =  new RedisHash({
            client: this.redisClient,
            hash: "hash_imsi_isocn"});
        return _hash_imsi_isocn;
    }.bind(this);

    var _hash_campaign_deficit = null;
    this.hash_campaign_deficit = function () {
        if (_hash_campaign_deficit)
            return _hash_campaign_deficit;
        _hash_campaign_deficit =  new RedisHash({
            client: this.redisClient,
            hash: "hash_campaign_deficit"});
        return _hash_campaign_deficit;
    }.bind(this);

    var _hash_imsi_isocn = null;
    this.hash_imsi_isocn = function () {
        if (_hash_imsi_isocn)
            return _hash_imsi_isocn;
        _hash_imsi_isocn =  new RedisHash({
            client: this.redisClient,
            hash: "hash_imsi_isocn"});
        return _hash_imsi_isocn;
    }.bind(this);





    // ============================  SETs

    var _setAllUsers = null;
    this.setAllUsers = function () {
        if (_setAllUsers)
            return _setAllUsers;
        _setAllUsers =  new RedisSet({
            client: this.redisClient,
            setKey: "set_all_users"});
        return _setAllUsers;
    }.bind(this);

    var _setAllPackagenames = null;
    this.setAllPackagenames = function () {
        if (_setAllPackagenames)
            return _setAllPackagenames;
        _setAllPackagenames =  new RedisSet({
            client: this.redisClient,
            setKey: "set_all_packagenames"});
        return _setAllPackagenames;
    }.bind(this);
    
    
    var _setDynamicModuleExcluded = null;
    this.setDynamicModuleExcluded = function () {
        if (_setDynamicModuleExcluded)
            return _setDynamicModuleExcluded;
        _setDynamicModuleExcluded = new RedisSet({
            client: this.redisClient,
            setKey: "set_dynamic_module_excluded"
        });
        return _setDynamicModuleExcluded;
    }.bind(this);

    // =======================Lists

    var _offers = null;
    this.offers = function (list) {
        if (_offers)
            return _offers;
        _offers = new RedisList({
            client: this.redisClient,
            list: list
        });
        return _offers;
    }.bind(this);

    // =======================Sorted Sets

    var _manufacturers_score = null;
    this.manufacturers_score = function () {
        if (_manufacturers_score)
            return _manufacturers_score;
        _manufacturers_score =  new RedisSortedSets({
            client: this.redisClient,
            setKey: "zset_manufacturers_score"});
        return _manufacturers_score;
    }.bind(this);

    var _phonemodel_score = null; //TODO check if this is for all requests .
    this.phonemodel_score = function () {
        if(_phonemodel_score)
            return _phonemodel_score;
        var _phonemodel_score =  new RedisSortedSets({
            client: this.redisClient,
            setKey: "zset_phonemodel_score_"});
        return _phonemodel_score;
    }.bind(this);



}

module.exports = MinimobRedisSiteRepository;