﻿var logger = require("../utils/logger");
var util = require('util');
var EventEmitter = require("events").EventEmitter;
var config = require('../config/config');




var MinimobMongoRepository = function (options) {
    this.connectionString = options.connectionString;
    this.mongoose = require('mongoose');
    this.master =  options.master||false;
    this.connected = false;
    this.settings = null;
    this.localCache=options.localCache;
    this.settingsIntervalFn = null;
    this.app = options.app;


    this.db = this.mongoose.createConnection(this.connectionString, { keepAlive: 1 });//this.mongoose.connection;

    this.db.on('error', function (err) {
        logger.error('MongoRepository connection error: ' + err);
        logger.debug('MongoRepository connection error: ' + err);
        this.connectionError = err;
        this.connected = false;
    }.bind(this));

    this.db.once('open', function () {
        var _this = this;
        logger.info("MongoRepository connection established : "+ this.connectionString);
        logger.debug("MongoRepository connection established : "+this.connectionString);
        this.connected = true;
        if(this.master) {
            this.app.emit('dbConnected');
            /*try {
                this.adservingsortingpolicy1 = this.db.model('adServingSortingPolicy', adServingSortingPolicySchema);
                this.adservingsettings().findOne({}).populate('defaultSortingPolicyId').exec(function (err, data) {
                    if (!err) {
                        var oData = data.toJSON();
                        _this.settings = oData;
                        _this.settingsInterval = oData.settingsInterval
                        _this.settingsIntervalFn = setInterval(function () {
                            _this.setSettings();
                        }.bind(this), oData.settingsInterval);

                    } else {
                        logger.info("Could not find Settings in Mongo Error  : " + this.connectionString);
                        logger.debug("Could not find Settings in Mongo Error : " + this.connectionString);
                    }
                });
            } catch (e) {
                logger.info("Could not Load Setting From Mongo : " + this.connectionString);
                logger.debug("Could not Load Setting From Mongo : " + this.connectionString);
            }*/
        }

    }.bind(this))

    this.db.on('reconnected', function () {
        logger.info("MongoRepository connection established:reconnected : " + this.connectionString);
        logger.debug("MongoRepository connection established:reconnected : " + this.connectionString);
        if(this.master) {
            //this.mongoCluster.reconnectSlaves();
            this.emit('reconnected');
        }
        this.connected = true;
    }.bind(this));

    //this.on('reconnectSlaves',function(data){console.log(data)});



    var userSchema = require('./MongoEntities/user.js');
    var sequencesSchema = require('./MongoEntities/sequences.js');
    var articlesSchema = require('./MongoEntities/articles.js');
    var categoriesSchema = require('./MongoEntities/categories.js');


    var _sequences = null;
    this.sequences = function () {
        if (_sequences)
            return _sequences;
        _sequences = this.db.model('sequences', sequencesSchema);
        return _sequences;
    }.bind(this);

    var _article = null;
    this.article = function () {
        if (_article)
            return _article;
        _article = this.db.model('article', articlesSchema);
        return _article;
    }.bind(this);

    var _category = null;
    this.category = function () {
        if (_category)
            return _category;
        _category = this.db.model('category', categoriesSchema);
        return _category;
    }.bind(this);

    var _user = null;
    this.user = function () {
        if (_user)
            return _user;
        _user = this.db.model('User', userSchema);
        return _user;
    }.bind(this);


    EventEmitter.call(this);

}

util.inherits(MinimobMongoRepository,EventEmitter);




// Public
module.exports = MinimobMongoRepository;