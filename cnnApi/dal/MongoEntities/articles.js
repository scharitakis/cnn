/**
 * Created by s.charitakis
 */

var mongoose = require('mongoose');
require('mongoose-long')(mongoose);
var  Schema = mongoose.Schema;
var CategoriesSchema = require('./categories')

var ArticlesSchema = new Schema({
    '_user_id': { type: String, default: "" },
    '_title':{ type: String, default: "" },
    '_date_created':{type: Date},
    '_description':{ type: String, default: "" },
    '_photos':[],
    '_category_id':{ type: String, default: "" },
    '_videos':[],
}, { collection: 'article',versionKey:false });


module.exports = ArticlesSchema;