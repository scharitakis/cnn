/**
 * Created by s.charitakis
 */

var mongoose = require('mongoose');
require('mongoose-long')(mongoose);
var  Schema = mongoose.Schema;

var CategoriesSchema = new Schema({
    '_title':{ type: String, default: "" },
    '_date_created':{type: Date},
    '_user_id': { type: String, default: "" },
    '_deleted':{ type: Boolean, default: false }
}, { collection: 'category',versionKey:false });


module.exports = CategoriesSchema;