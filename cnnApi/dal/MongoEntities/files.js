/**
 * Created by s.charitakis
 */

var mongoose = require('mongoose');
var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;

var File = function(options){
    this.db = options.db;
}


File.prototype.readFile = function(fileName,clb){
    var gfs = Grid(this.db);
    var buffer = "";
    //ObjectId("54fda2a0ff5edec01ebe8d1f")
    gfs.findOne({ _filename: fileName }, function(err, result) {
        if (err || result==null)
        {
            return clb('error in finding the file: "'+fileName+'" in gridfs',null);
        }
        var readstream = gfs.createReadStream({_id:result._id});
        if(result._content_type != "text/html")
        {
            readstream.setEncoding('binary');
        }else{
            readstream.setEncoding('utf8');
        }

        readstream.on("data", function (chunk) {
            buffer += chunk;
        });

        readstream.on("end", function () {
            result.content  = buffer
            return clb(null,result);
        });
    });

}

File.prototype.readFileById = function(objid,clb){
    var gfs = Grid(this.db);
    var buffer = "";

    gfs.findOne({ _id: objid }, function(err, result) {
        if (err || result==null)
        {
            return clb('error in finding the file: "'+fileName+'" in gridfs',null);
        }
        var readstream = gfs.createReadStream({_id:result._id});
        if(result._content_type != "text/html")
        {
            readstream.setEncoding('binary');
        }else{
            readstream.setEncoding('utf8');
        }

        readstream.on("data", function (chunk) {
            buffer += chunk;
        });

        readstream.on("end", function () {
            result.content  = buffer
            return clb(null,result);
        });
    });

}


module.exports = File;