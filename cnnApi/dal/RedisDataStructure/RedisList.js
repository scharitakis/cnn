/**
 * Created by s.charitakis
 */
var RedisDBEntityList = function(options){
    this.max_queue = (options && options.max_queue ? options.max_queue : null);
    this.list = (options && options.list ? options.list : null);
    this.redisClient = (options && options.client ? options.client : null);

}

RedisDBEntityList.prototype.llen = function(callback)
{
    this.redisClient.llen(this.list, function (err, reply) {
        return callback(err, reply);
    });
}


RedisDBEntityList.prototype.lpush = function(value,callback)
{

    this.redisClient.lpush(this.list, value, function (err, reply) {
        return callback(err, reply);
    });

}

RedisDBEntityList.prototype.rpush = function(value,callback)
{
    var _this = this
    this.llen(function(err,res){
       if(err){
           callback(err, null)
       }
       if(res<_this.max_queue) {
            _this.redisClient.rpush(_this.list, value,  function (err, reply) {
                return callback(err, reply);
            });
        }
    })
}

RedisDBEntityList.prototype.lrange = function(start,end,callback)
{

    this.redisClient.lrange(this.list, start,end, function (err, reply) {
        return callback(err, reply);

    });

}

RedisDBEntityList.prototype.rpushMultiEx = function(value,callback)
{
    var multi = this.redisClient.multi();
    for(var i=0;i<value.length;i++){
       // multi.rpush(this.list, value[i], function (err, reply) { });
       multi.rpush(this.list, JSON.stringify(value[i]), function (err, reply) { });
    }
    multi.expire(this.list, 1200, function (err, reply){});
    multi.exec(function (err, replies) {
        if (!err) {
            return callback(null,true);
        }
        else {
            return callback(err, null);
        }
    });
}

RedisDBEntityList.prototype.lpop = function(callback)
{
    this.redisClient.set(this.list, function (err, reply) {
        return callback(err, reply);
    });
}

RedisDBEntityList.prototype.rpop = function(callback)
{
    this.redisClient.rpop(this.list, function (err, reply) {
        return callback(err, reply);
    });
}


module.exports = RedisDBEntityList;