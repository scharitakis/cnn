/**
 * Created by s.charitakis
 */

var RedisDBEntitySet = function(options){
    this.redisClient = (options && options.client ? options.client : null);
    this.setKey = (options && options.setKey ? options.setKey : null);
}



RedisDBEntitySet.prototype.sAdd = function(value,callback)
{
    this.redisClient.sadd(this.setKey,value, function (err, replies) {
        return callback(err,replies)
    })
}

RedisDBEntitySet.prototype.sMembers = function(value,callback)
{
    this.redisClient.sMembers(this.setKey,function (err, replies) {
        return callback(err,replies)
    })
}


RedisDBEntitySet.prototype.SDIff = function(setIds,callback)
{
    this.redisClient.sdiff(this.setKey,setIds, function (err, replies) {
        return callback(err,replies)
    })
}

RedisDBEntitySet.prototype.sismember = function (value, callback) {
    this.redisClient.sismember(this.setKey, value, function (err, replies) {
        return callback(err, replies)
    })
}


module.exports = RedisDBEntitySet;