/**
 * Created by s.charitakis on 3/13/2015.
 */


var RedisDBEntitySortedSets = function(options){
    this.redisClient = (options && options.client ? options.client : null);
    this.setKey = (options && options.setKey ? options.setKey : null);
}

RedisDBEntitySortedSets.prototype.zAdd = function(score,member,callback/*, key */)
{
    var key = this.setKey
    if(arguments.length ==4)
    {
        key = this.setKey+arguments[3]
    }
    this.redisClient.zadd(key,score,member, function (err, replies) {
    return callback(err,replies)
})
}




RedisDBEntitySortedSets.prototype.zIncrBy = function(incrby,member,callback/*, key */)
{
    var key = this.setKey
    if(arguments.length ==3)
    {
        key = this.setKey+arguments[2]
    }
    this.redisClient.zincrby(key,incrby,member, function (err, replies) {
        return callback(err,replies)
    })
}

RedisDBEntitySortedSets.prototype.zScore = function(member,callback/*, key */)
{
    var key = this.setKey
    if(arguments.length ==3)
    {
        key = this.setKey+arguments[2]
    }
    this.redisClient.zscore(key,member, function (err, replies) {
        return callback(err,replies)
    })
}

RedisDBEntitySortedSets.prototype.zRange = function(start,stop,callback/*, key */)
{
    //WITHSCORES
    var key = this.setKey
    if(arguments.length ==4)
    {
        key = this.setKey+arguments[3]
    }
    this.redisClient.zrange(key,start,stop, function (err, replies) {
        return callback(err,replies)
    })
}

RedisDBEntitySortedSets.prototype.zRank = function(member,callback/*, key */)
{
    var key = this.setKey
    if(arguments.length ==3)
    {
        key = this.setKey+arguments[2]
    }
    this.redisClient.zrank(key,member,function (err, replies) {
        return callback(err,replies)
    })
}

RedisDBEntitySortedSets.prototype.zRevRank = function(member,callback/*, key */)
{
    var key = this.setKey
    if(arguments.length ==3)
    {
        key = this.setKey+arguments[2]
    }
    this.redisClient.zrevrank(key,member,function (err, replies) {
        return callback(err,replies)
    })
}




module.exports = RedisDBEntitySortedSets;