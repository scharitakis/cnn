/**
 * Created by s.charitakis on 1/27/2015.
 */


var logger = require("../utils/logger");
var RedisKeyValue = require('./RedisDataStructure/RedisKeyValue');
var RedisSortedSets = require('./RedisDataStructure/RedisSortedSets');


var MinimobRedisLocalRepository = function (options) {
    var port = (options && options.port)?options.port:null;
    var host = (options && options.host)?options.host:null;

    this.port = port;
    this.host = host;
    this.redis = require('redis');
    this.redisClient = this.redis.createClient(this.port, this.host);

    this.redisClient.on('connect', function () {
        logger.info("Redis Local connection established");
        logger.debug("Redis Local connection established");
        this.connected = true;
    }.bind(this));

    this.redisClient.on('error', function (err) {
        logger.error('Redis Local connection error: ' + err);
        this.connectionError = err;
        this.connected = false;
    }.bind(this));
    
    
    
    var _reqThreshold = null;
    this.reqThreshold = function () {
        if (_reqThreshold)
            return _reqThreshold;
        _reqThreshold = new RedisKeyValue({ client: this.redisClient });
        return _reqThreshold;
    }.bind(this);

    var _manufacturer_score = null;
    this.manufacturer_score = function () {
        if (_manufacturer_score)
            return _manufacturer_score;
        _manufacturer_score = new RedisSortedSets({
            client: this.redisClient,
            setKey: "zset_manufacturers_score"
        });
        return _manufacturer_score;
    }.bind(this);

    var _manufacturer = null;
    this.manufacturer = function () {
        if (_manufacturer)
            return _manufacturer;
        _manufacturer = new RedisKeyValue({ client: this.redisClient });
        return _manufacturer;
    }.bind(this);

    var _phonemodel_score = null;
    this.phonemodel_score = function () {
        if (_phonemodel_score)
            return _phonemodel_score;
        _phonemodel_score =  new RedisKeyValue({ client: this.redisClient});
        return _phonemodel_score;
    }.bind(this);
    
    var _phonemodel = null;
    this.phonemodel = function () {
        if (_phonemodel)
            return _phonemodel;
        _phonemodel = new RedisKeyValue({ client: this.redisClient });
        return _phonemodel;
    }.bind(this);


    //incr_hostcounter

    var _incr_hostcounter = null;
    this.incr_hostcounter = function () {
        if(_incr_hostcounter)
            return _incr_hostcounter;
        var _incr_hostcounter = new RedisKeyValue({ client: this.redisClient});
        return _incr_hostcounter;
    }.bind(this);


}

module.exports = MinimobRedisLocalRepository;