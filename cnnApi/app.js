﻿var express = require('express');
var cors = require('cors')
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var uuid = require("./utils/uuid");

var auth = require('./lib/middleware/auth');

var routes_config = require('./config/routes_config');
var app_config = require('./config/app_config');



var app = express();
app.use(cors());
var appConfig = app_config(app);
//var settings  = new settingsLogic({db:appConfig,app:app});

// view engine setup
var ECT = require('ect');
var ectRenderer = ECT({ watch: true, root: __dirname + '/views', ext : '.ect' });
app.set('view engine', 'ect');
app.engine('ect', ectRenderer.render);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next){
  var remoteIps = (req.ips.length)?req.ips:false || req.connection.remoteAddress;
  if(typeof remoteIps === "string"){
    remoteIps = remoteIps.replace('::ffff:',"");
    if(remoteIps.indexOf("::1")>-1){
      remoteIps = "127.0.0.1";
    }
  }
  req.remoteIP = remoteIps;
  req.sessionLogId = Math.uuid();
  next();
})

app.use(auth);
routes_config(app,appConfig);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});



// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

if (process.platform === "win32") {
    var rl = require("readline").createInterface({
        input: process.stdin,
        output: process.stdout
    });
    
    rl.on("SIGINT", function () {
        process.emit("SIGINT");
    });
}

process.on("SIGINT", function () {
    var db = app.get("app_config");
    var connections = Object.keys(db);
    console.log(connections)
    for (var i = 0; i < connections.length; i++) {
        console.log(connections[i])
        if (connections[i] == "mongoCluster") {
            console.log("closing Mongo Connections")
            db[connections[i]].masterRepository.mongoose.connection.close(function () { console.log("closing Mongo Master") });
            var slaveRepositories = db[connections[i]].slaveRepositories
            for (var d = 0; d < slaveRepositories.length; d++) {
                db[connections[i]].slaveRepositories[d].mongoose.connection.close(function () { console.log("closing Mongo Slave") });
            }
        }
        else if (["redisSiteRepository", "redisCacheRepository", "redisLocalRepository"].indexOf(connections[i]) > -1) {
            console.log("closing Redis Connections")
            db[connections[i]].redisClient.end();
        }
        else if (connections[i] == "redisSharding") {
            console.log("closing Redis Shard Connections")
            var clients = Object.keys(db[connections[i]].clients_map);
            for (var d = 0; d < clients.length; d++) {
                connections[i].clients_map[clients[d]].redisClient.end();
            }
        }
    }
    //graceful shutdown
    process.exit();
});


module.exports = app;
