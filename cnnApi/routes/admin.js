var express = require('express');
var router = express.Router();
var wait = require('wait.for');




router.use(function(req, res, next) {
  req.logMeta = {ip:req.remoteIP,hostName:req.hostname,user:req.body.imei,facility:"Controller",sequence:0,session:req.sessionLogId};
  next();
});


router.get('/', function(req, res, next) {
  if(!req.user){
    res.redirect('/')
  }
  else if(req.user.aud =="admin@cnnApi.com"){
    res.render('admin',{});
  }else{
    res.redirect('/cncApp/')
  }


});


module.exports = router;
