var express = require('express');
var router = express.Router();
var wait = require('wait.for');
var logger = require("../utils/logger");
var multer = require('multer');
var uuid = require('uuid');
var path = require('path');



var allowedTypes =['image/jpeg','video/avi','video/mp4','video/quicktime'];


function fileFilter (req, file, cb) {

    // The function should call `cb` with a boolean
    // to indicate if the file should be accepted

    // To reject this file pass `false`, like so:
    //cb(null, false)
    console.log('this is the file ',file);

    if(allowedTypes.indexOf(file.mimetype)>-1)
    {
        cb(null, true)
    }
    else{
        cb(null, false)
    }
    // To accept the file pass `true`, like so:
    //cb(null, true)

    // You can always pass an error if something goes wrong:
    //cb(new Error('I don\'t have a clue!'))

}




var AddArticlesControllerLogic = require('../bll/addArticlesControllerLogic');
var ArticlesControllerLogic = require('../bll/articlesControllerLogic');
var DeleteArticlesControllerLogic = require('../bll/deleteArticlesControllerLogic');


router.use(function(req, res, next) {
    req.logMeta = {ip:req.remoteIP,hostName:req.hostname,user:req.body.imei,facility:"Controller",sequence:0,session:req.sessionLogId};
    next();
});


router.get('/', function (req, res, next) {
    wait.launchFiber(
        function (request, response) {
            var app_config = request.app.get('app_config');
            var db = {
                dbMongo: app_config.mongoCluster.randomSlaveRepository(),
                localcache: app_config.localcache
            }

            var result = {};

            try {
                var articlesController = new ArticlesControllerLogic( {
                    logMeta:request.logMeta,
                    db:db
                });
                result = articlesController.startArticlesController(request,response);
            }
            catch (err) {
                var msg = (err.message)?err.message:err;
                if(err.name == "CastError" ){
                    msg = "The value " + err.value +" is not Valid"
                }
                result = {error:msg};
                logger.error(msg,request.logMeta);
            }
            finally {
                var msg = "Response for Upload Controller: PATH:"+request.path+" Request Method:"+request.method+" BODY:"+ JSON.stringify(result);
                if(result.error)
                {
                    response.status(500)
                    response.send(result.error)
                    //response.status(500);
                    //response.send('Internal server error. Please contact cnn administrator(s)!');
                }else {
                    response.json(result);
                }
            }


        }, req, res);
});

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        console.log(req)
        console.log(file)
        cb(null, uuid.v4()+'-' +Date.now()+ path.extname(file.originalname))
    }
})

var upload = multer({ storage: storage,fileFilter:fileFilter,limits:{fileSize:20000*1024}}).array('uploads', 4)

router.post('/',function (req, res, next) {
    upload(req, res, function (err) {
        if (err) {
            console.log(err.code);
            if(err.code =="LIMIT_FILE_SIZE"){
                res.send({err:true,msg:"File size too large"})
            }else{
                res.send({err:true,msg:"An error has occured while uploading"})
            }

        }else{
            wait.launchFiber(
                function (request, response) {
                    var app_config = request.app.get('app_config');
                    var db = {
                        dbMongo: app_config.mongoCluster.randomSlaveRepository(),
                        localcache: app_config.localcache
                    }

                    var result = {};

                    try {
                        var addArticlesController = new AddArticlesControllerLogic( {
                            logMeta:request.logMeta,
                            db:db
                        });
                        result = addArticlesController.startAddArticlesController(request,response);
                    }
                    catch (err) {
                        var msg = (err.message)?err.message:err;
                        if(err.name == "CastError" ){
                            msg = "The value " + err.value +" is not Valid"
                        }
                        result = {error:msg};

                        logger.error(msg,request.logMeta);
                    }
                    finally {
                        var msg = "Response for Upload Controller: PATH:"+request.path+" Request Method:"+request.method+" BODY:"+ JSON.stringify(result);
                        if(result.error)
                        {
                            response.status(500)
                            response.send(result)
                            //response.status(500);
                            //response.send('Internal server error. Please contact cnn administrator(s)!');
                        }else {
                            response.json(result);
                        }
                    }


                }, req, res);
        }
    })



});

router.delete('/', function (req, res, next) {
    wait.launchFiber(
        function (request, response) {
            var app_config = request.app.get('app_config');
            var db = {
                dbMongo: app_config.mongoCluster.randomSlaveRepository(),
                localcache: app_config.localcache
            }

            var result = {};

            try {
                var deleteArticlesController = new DeleteArticlesControllerLogic( {
                    logMeta:request.logMeta,
                    db:db
                });
                result = deleteArticlesController.startDeleteArticlesController(request,response);
            }
            catch (err) {
                var msg = (err.message)?err.message:err;
                if(err.name == "CastError" ){
                    msg = "The value " + err.value +" is not Valid"
                }
                result = {error:msg};
                logger.error(msg,request.logMeta);
            }
            finally {
                var msg = "Response for Upload Controller: PATH:"+request.path+" Request Method:"+request.method+" BODY:"+ JSON.stringify(result);
                if(result.error)
                {
                    response.status(500)
                    response.send(result)
                    //response.status(500);
                    //response.send('Internal server error. Please contact cnn administrator(s)!');
                }else {
                    response.json(result);
                }
            }


        }, req, res);
});

module.exports = router;
