var express = require('express');
var router = express.Router();
var wait = require('wait.for');




router.use(function(req, res, next) {
    req.logMeta = {ip:req.remoteIP,hostName:req.hostname,user:req.body.imei,facility:"Controller",sequence:0,session:req.sessionLogId};
    next();
});


router.get('/', function(req, res, next) {
    res.clearCookie("x-cnnApi-authorization");
    res.redirect('/login');
    return
});


module.exports = router;
