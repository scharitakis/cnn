var express = require('express');
var router = express.Router();
var LoginUserControllerLogic = require('../bll/loginUserControllerLogic');


/* GET users listing. */
router.post('/', function(req, res, next) {
    if(req.user){
        res.send("You are already logged in")
        res.end()
    }
    else{

        wait.launchFiber(
            function (request, response) {
                var app_config = request.app.get('app_config');
                var db = {
                    dbMongo: app_config.mongoCluster.randomSlaveRepository(),
                    localcache: app_config.localcache
                }

                var result = {};

                try {
                    var loginUserController = new LoginUserControllerLogic( {
                        logMeta:request.logMeta,
                        db:db
                    });
                    result = loginUserController.startLoginUserController(request,response);
                }
                catch (err) {
                    var msg = (err.message)?err.message:err;
                    result = {error:msg};
                    logger.error(msg,request.logMeta);
                }
                finally {
                    if(result.error)
                    {
                        response.send(result.error)
                        //response.status(500);
                        //response.send('Internal server error. Please contact cnn administrator(s)!');
                    }else {
                        response.json(result);
                    }
                }


            }, req, res);



    }





});

module.exports = router;
