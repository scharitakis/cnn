var express = require('express');
var router = express.Router();
var wait = require('wait.for');
var logger = require("../utils/logger");


var AddCategoriesControllerLogic = require('../bll/addcategoriesControllerLogic');
var DeleteCategoriesControllerLogic = require('../bll/deleteCategoriesControllerLogic');
var UpdateCategoriesControllerLogic = require('../bll/updateCategoriesControllerLogic');
var CategoriesControllerLogic = require('../bll/categoriesControllerLogic');

router.use(function(req, res, next) {
    req.logMeta = {ip:req.remoteIP,hostName:req.hostname,user:req.body.imei,facility:"Controller",sequence:0,session:req.sessionLogId};
    next();
});

router.get('/', function (req, res, next) {
    wait.launchFiber(
        function (request, response) {
            var app_config = request.app.get('app_config');
            var db = {
                dbMongo: app_config.mongoCluster.randomSlaveRepository(),
                localcache: app_config.localcache
            }

            var result = {};

            try {
                var categoriesController = new CategoriesControllerLogic( {
                    logMeta:request.logMeta,
                    db:db
                });
                result = categoriesController.startCategoriesController(request,response);
            }
            catch (err) {
                var msg = (err.message)?err.message:err;
                if(err.name == "CastError" ){
                    msg = "The value " + err.value +" is not Valid"
                }
                result = {error:msg};
                logger.error(msg,request.logMeta);
            }
            finally {
                var msg = "Response for Upload Controller: PATH:"+request.path+" Request Method:"+request.method+" BODY:"+ JSON.stringify(result);
                if(result.error)
                {
                    response.status(500)
                    response.send(result)
                    //response.status(500);
                    //response.send('Internal server error. Please contact cnn administrator(s)!');
                }else {
                    response.json(result);
                }
            }


        }, req, res);
});

router.post('/', function (req, res, next) {
    wait.launchFiber(
        function (request, response) {
            var app_config = request.app.get('app_config');
            var db = {
                dbMongo: app_config.mongoCluster.randomSlaveRepository(),
                localcache: app_config.localcache
            }

            var result = {};

            try {
                var addcategoriesController = new AddCategoriesControllerLogic( {
                    logMeta:request.logMeta,
                    db:db
                });
                result = addcategoriesController.startAddCategoriesController(request,response);
            }
            catch (err) {
                var msg = (err.message)?err.message:err;
                if(err.name == "CastError" ){
                    msg = "The value " + err.value +" is not Valid"
                }
                result = {error:msg};
                logger.error(msg,request.logMeta);
            }
            finally {
                var msg = "Response for Upload Controller: PATH:"+request.path+" Request Method:"+request.method+" BODY:"+ JSON.stringify(result);
                if(result.error)
                {
                    //response.send(result.error)
                    response.status(500);
                    response.send(result);
                }else {
                    response.json(result);
                }
            }


        }, req, res);
});

router.delete('/', function (req, res, next) {
    wait.launchFiber(
        function (request, response) {
            var app_config = request.app.get('app_config');
            var db = {
                dbMongo: app_config.mongoCluster.randomSlaveRepository(),
                localcache: app_config.localcache
            }

            var result = {};

            try {
                var deletecategoriesController = new DeleteCategoriesControllerLogic( {
                    logMeta:request.logMeta,
                    db:db
                });
                result = deletecategoriesController.startDeleteCategoriesController(request,response);
            }
            catch (err) {
                var msg = (err.message)?err.message:err;
                if(err.name == "CastError" ){
                    msg = "The value " + err.value +" is not Valid"
                }
                result = {error:msg};
                logger.error(msg,request.logMeta);
            }
            finally {
                var msg = "Response for Upload Controller: PATH:"+request.path+" Request Method:"+request.method+" BODY:"+ JSON.stringify(result);
                if(result.error)
                {
                    //response.send(result.error)
                    response.status(500);
                    response.send(result);
                }else {
                    response.json(result);
                }
            }


        }, req, res);
});

router.put('/', function (req, res, next) {
    wait.launchFiber(
        function (request, response) {
            var app_config = request.app.get('app_config');
            var db = {
                dbMongo: app_config.mongoCluster.randomSlaveRepository(),
                localcache: app_config.localcache
            }

            var result = {};

            try {
                var updateCategoriesController = new UpdateCategoriesControllerLogic( {
                    logMeta:request.logMeta,
                    db:db
                });
                result = updateCategoriesController.startUpdateCategoriesController(request,response);
            }
            catch (err) {
                var msg = (err.message)?err.message:err;
                if(err.name == "CastError" ){
                    msg = "The value " + err.value +" is not Valid"
                }
                result = {error:msg};
                logger.error(msg,request.logMeta);
            }
            finally {
                var msg = "Response for Upload Controller: PATH:"+request.path+" Request Method:"+request.method+" BODY:"+ JSON.stringify(result);
                if(result.error)
                {
                    //response.send(result.error)
                    response.status(500);
                    response.send(result);
                }else {
                    response.json(result);
                }
            }


        }, req, res);
});


module.exports = router;
