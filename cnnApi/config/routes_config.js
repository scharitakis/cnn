/**
 * Created by s.charitakis
 */

var routes = function(app,app_config){

    app.set("app_config",app_config);


    //Site Routes
    var routes_admin = require('../routes/admin');
    var routes_index = require('../routes/index');
    var routes_logout = require('../routes/logout');

    app.use('/', routes_index);
    app.use('/admin', routes_admin);
    app.use('/logout', routes_logout);


    //API Routes
    var routes_articles = require('../routes/articles');
    var routes_categories = require('../routes/categories');

    var routes_loginApi = require('../routes/loginApi');

    app.use('/api/v1/articles',routes_articles)
    app.use('/api/v1/categories',routes_categories)
    app.use('/api/v1/loginApi',routes_loginApi)

};

module.exports = routes;