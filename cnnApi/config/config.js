﻿/**
 * Created by s.charitakis
 */

var configNew = (function() {

    var currentEnv = process.env.NODE_ENV || "local";
    var secret = "testme";

    var servers ={
        development:"http://api.cnn.gr",
        production:"http://api.cnn.gr",
        local:"http://localhost"
    }
    var cachingTTL = 1200;

    var mongo = {
        production: {
            mongoSlaves: ['mongodb://localhost/cnn'],
            mongoMaster: 'mongodb://localhost/cnn'
        },
        development: {
            mongoSlaves: ['mongodb://localhost/cnn'],
            mongoMaster: 'mongodb://localhost/cnn'
        },
        local: {
            mongoSlaves: ['mongodb://192.168.99.100:32768/cnn'],
            mongoMaster: 'mongodb://192.168.99.100:32768/cnn'
        }
    }

    var logger =  {
        production:{
            "log_dir" : "./log/all-logs.log",
                "redis_list" : "list_logstash",
                "redis" : {host:"localhost",port:6379},
            "max_queue" : 200000
        },
        development:{
            //"log_dir" : "/var/log/OffervaultConsumerApi/all-logs.log",
            "log_dir" : "./log/all-logs.log",
                "redis_list" : "list_logstash",
                "redis" : {host:"localhost",port:6379},
            "max_queue" : 200000
        },
        local:{
            //"log_dir" : "/var/log/OffervaultConsumerApi/all-logs.log",
            "log_dir" : "./log/all-logs.log",
            "redis_list" : "list_logstash",
            "redis" : {host:"localhost",port:6379},
            "max_queue" : 200000
        }
    }

    return {
        mongo:mongo[currentEnv],
        cachingTTL:cachingTTL,
        logger:logger[currentEnv],
        servers:servers[currentEnv],
        secret:secret
    }


})()


module.exports = configNew;