/**
 * Created by s.charitakis
 */
var config = require("./config.js");
var fs = require('fs');

var MinimobMongoCluster = require('../dal/MinimobMongoCluster');
//var MinimobRedisSiteRepository = require('../dal/MinimobRedisSiteRepository');
//var MinimobRedisCacheRepository = require('../dal/MinimobRedisCacheRepository');
//var MinimobRedisSharding = require('../dal/MinimobRedisSharding');
//var MinimobIPtoLocation = require('../dal/MinimobIPtoLocation');
var NodeCache = require("node-cache");

//var MinimobRedisLocalRepository = require('../dal/MinimobRedisLocalRepository');

var instance = null

var app_config = function(app) {

    if(!instance) {
        var localCache = new NodeCache();
        instance = {
            //ipToLocation: new MinimobIPtoLocation(config.ip2location_endpoints),

            mongoCluster: new MinimobMongoCluster({
                master: config.mongo.mongoMaster,
                slaves: config.mongo.mongoSlaves,
                localCache: localCache,
                app: app
            }),

            //redisSharding: new MinimobRedisSharding(config.redis.development.shards),
            //redisSiteRepository: new MinimobRedisSiteRepository(config.redis.development.site),
            //redisCacheRepository: new MinimobRedisCacheRepository(config.redis.development.cache),
            //redisLocalRepository: new MinimobRedisLocalRepository(config.redis.development.localRedis),
            localcache: localCache
        };
    }
     return instance;

}




module.exports = app_config;